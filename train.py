import sys

sys.path.append("/ptmp/mvondrak/kQEqDipoles/qpac/")

from qpac.descriptors import quipSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq

from ase.units import Hartree
from ase.io import read, write
import numpy as np


mols = read(f"pubchemCHNOF.xyz@:",format="extxyz")
addPeriodicity(mols)
import random
random.seed(10)
random.shuffle(mols)

size_train = 50000
sigma = 0.1
train_set = mols[:size_train]
soap_descriptor = quipSOAP(STRsoap="soap cutoff=4.0 l_max=4 n_max=6 atom_sigma=1.0 normalize=T n_species=5 species_Z={1, 6, 8, 7, 9} n_Z=5 Z={1, 6, 8, 7, 9}", species=['H', 'C', 'O', 'N', 'F'])
qpac_kernel = qpacKernel(Descriptors=soap_descriptor,
                     training_set=train_set,
                     training_set_charges=[0 for a in train_set],
                     sparse = True,
                     perEl = True,
                     sparse_method = "random",
                     sparse_count=2000)
my_kqeq = kernel_qeq(Kernel=qpac_kernel,
            scale_atsize=1.0/np.sqrt(2.0),
            radius_type="qeq")
my_kqeq.train(
    target_sigmas =[sigma],#,0.01], 
    targets = ["dipole"],
    dipole_keyword = "dipole_vector")
my_kqeq.save_kQEq(nameW=f"KQEqWeights{sigma}_{size_train}",nameS=f"KQEqSOAP{sigma}_{size_train}",nameEl=f"KQEqEl{sigma}_{size_train}")
