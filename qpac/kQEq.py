import numpy as np
from ase.io import *
from ase.data import covalent_radii
from ase.units import Bohr,Hartree
from qpac.qeq import charge_eq
from qpac.utils import concB,get_forces_atomic,get_forces_eV,_block_diag_rect,_block_diag_rectR,_block_diag,_get_R, get_energies_atomic, get_charges,stack_matrices_diagonally,create_L_matrix,get_energies_perAtom_eV,get_energies_eV
from scipy.optimize import minimize

from sklearn.metrics import mean_squared_error
import time

'''
In the following code, I will use annotation
N - number of training points, this correspond to the number of atoms in all training systems
M - number of representative points, again, each is an atom (probably picked by CUR) from training system
P - number of atoms in the structure you want to predict
S - number of systems
The dimensions of matrices are always written as they are for a single system problem, otherwise they are in block matrices
'''


class kernel_qeq():
    """Class for Kernel Charge Equilibration Models. Do not contain training function. This class should be used only for prediction with pretained models

    Parameters
    ----------
        Kernel: obj
            Kernel object with the function kernel_matrix
        scale_atsize: float
            Scaling factor for covalent radii, used to define atom sizes
        radius_type: string
            'rcov' or 'qeq' (default)
        hard_lib : dictionary
            dictionary of hardness values for elements in eV
        periodic : bool
            Set up periodicity of ALL structures (so far, combined periodicity cannot be used)
    """
    def __init__(self, Kernel=None, 
                 scale_atsize=1.0,
                 radius_type='qeq',
                 calculate_kernel_forces=True,
                 include_LJ=False, 
                 hard_lib=None,
                 periodic = False,
                 Kmm_jiter = 0.0000001):
        if Kernel == None:
            print("Specify kernel!")
            exit()
        self.scale_atsize = scale_atsize
        self.calculate_kernel_forces = calculate_kernel_forces
        self.LJ = include_LJ
        self.Kernel       = Kernel
        self.radius_type  = radius_type
        self.weights      = None
        self.hard_lib     = hard_lib
        self.periodic = periodic
        if self.Kernel.training_set[0] is not None:
            self.train_mol_sizes = np.array([len(mol) for mol in self.Kernel.training_set])
            # A (N+1,N+1) - hardness matrix, X (N+1,N) - matrix to remove lambda from q_bar, O (N,) - vector of zeros and total charges
            # self.A_train, self.X_train, self.O_train = self._build_A_X_O(mols=self.Kernel.training_set,systems_charges=self.Kernel.training_system_charges)
            self.elements = self.Kernel.elements
            self.K_nms, K_mms = self.Kernel.kernel_matrices(kerneltype='training')
            for K_mm in K_mms:
                K_mm += np.eye(K_mm.shape[0])*Kmm_jiter
            self.K_mms = K_mms
        
    def calculateEQ(self, mol, charge=0):
        """Calculates charges, energy, forces and dipole_vector for single atoms object.

        Parameters
        ----------
            mol : obj
                Atoms object
            charge : int
                Charge of mol

        Returns
        -------
            results: dict
                Dictionary of results

        """ 
        Ks = self.Kernel.kernel_matrices(mol_set1 = [mol],kerneltype="predicting")
        nAt = len(mol)
        eneg = np.zeros(nAt)
        KsStuck = np.hstack(Ks)#/Bohr
        eneg = np.matmul(KsStuck,self.weights)#/Hartree
        qe = charge_eq(mol,Qtot=charge,eneg=eneg,scale_atsize=self.scale_atsize,DoLJ=self.LJ,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
        qe.calc_Charges()
        qe.calc_Eqeq()
        qe.calc_Fqeq()
        charges  = qe.q
        energy  = qe.E*Hartree
        results = {'charges':charges,'energy':energy}
        return results

    def calculate(self, mol, charge=0):
        """Calculates charges, energy, forces and dipole_vector for single atoms object.

        Parameters
        ----------
            mol : obj
                Atoms object
            charge : int
                Charge of mol

        Returns
        -------
            results: dict
                Dictionary of results

        """
        Ks,dKdrs  = self.Kernel._calculate_function(mol_set1 = mol) 
        nAt = len(mol)
        eneg = np.zeros(nAt)
        eneg_drj = np.zeros((len(mol),3,len(mol)))
        KsStuck = np.hstack(Ks)#/Bohr
        dKdrs = np.array(dKdrs)
        eneg = np.matmul(KsStuck,self.weights)#/Hartree
        qe = charge_eq(mol,Qtot=charge,eneg=eneg,scale_atsize=self.scale_atsize,DoLJ=self.LJ,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
        qe.calc_Charges()
        qe.calc_Eqeq()
        qe.calc_Fqeq()
        charges  = qe.q
        energy  = qe.E*Hartree
        f_qeq  = qe.f
        enegs_drj = np.einsum("abcd,d->abc",dKdrs.squeeze(),self.weights)
        '''
        previous line is this in for loops, this is here just for writing reasons, if it is in the main branch I have holes in my brain
        i,d,j,m = temp.shape
        temp_edrj = np.zeros([i,d,j])
        for ind_i in range(i):
            for ind_d in range(d):
                for ind_j in range(j):
                    for ind_m in range(m):
                        temp_edrj[ind_i,ind_d,ind_j] += temp[ind_i,ind_d,ind_j,ind_m]*self.weights[ind_m]
        '''
        f_eneg = np.einsum("idj,j->id",enegs_drj,charges)
        # print(f_eneg/f_qeq)
        forces = (Hartree/Bohr) * (-f_eneg + f_qeq)
        
        # temp = dKdrs.squeeze()
        # charges_enegs_drj = np.einsum("k,ijk->ijk",charges,enegs_drj)
        # f_eneg = -np.sum(charges_enegs_drj, axis=2)
        # temp1 = np.einsum("idjm,j->idm",temp,charges)
        # temp = -np.einsum("idm,m->id",temp1,self.weights)

        '''
        forces  = qe.f*(Hartree/Bohr)
        f_k  = np.zeros((nAt,3))
        for j in range(nAt):
            for direction in range(3):
                for i in range(nAt):
                    f_k[j,direction] += -charges[i]*eneg_drj[j][direction][i]
        forces += f_k*(Hartree/Bohr)
        '''
        results = {'charges':charges,'energy':energy,'forces':forces}
        # results = {'charges':charges,'energy':energy,'forces':forces}
        return results


    def save_kQEq(self, nameW="KQEqWeights",nameS="KQEqSOAP",nameEl="KQEqEl"):
        """Saves trained model weights, SOAP vector of the representative set, and array of elements corresponding to representatve set.

        Parameters
        ----------
            nameW : string
                Name of the file of weights
            nameS : string
                Name of the file of SOAP vectors
            nameEl : string
                Name of the file of elements
        """
        print("Saving kQEq model")
        np.save(nameW, self.weights)
        for iddesc in range(len(self.Kernel.Descriptors)):
            np.save(f"{nameS}_{iddesc}" , self.Kernel.representing_set_descriptors[iddesc])
            np.save(f"{nameEl}_{iddesc}", self.Kernel.representing_set_elements[iddesc])
    
    def load_kQEq(self, nameW="KQEqWeights",nameS="KQEqSOAP",nameEl="KQEqEl"):
        """Loads trained model weights, SOAP vector of the representative set, and array of elements corresponding to representatve set.

        Parameters
        ----------
            nameW : string
                Name of the file of weights
            nameS : string
                Name of the file of SOAP vectors
            nameEl : string
                Name of the file of elements
        """
        print("Loading kQEq model")
        self.weights = np.load(f"{nameW}.npy")
        self.Kernel.representing_set_descriptors = []
        self.Kernel.representing_set_elements = []
        for iddesc in range(len(self.Kernel.Descriptors)):
            self.Kernel.representing_set_descriptors.append(np.load(f"{nameS}_{iddesc}.npy"))
            self.Kernel.representing_set_elements.append(np.load(f"{nameEl}_{iddesc}.npy"))
        self.Kernel.repres_descs = []
        for iddes in range(len(self.Kernel.Descriptors)):
            repres_descs_temp = {}
            ldesc = len(self.Kernel.representing_set_descriptors[iddes][0])
            for el in self.Kernel.elements:
                repres_descs_temp[el] = []
                for countRepre in range(len(self.Kernel.representing_set_descriptors[iddes])):
                    if el == self.Kernel.representing_set_elements[iddes][countRepre]:
                        repres_descs_temp[el].append(self.Kernel.representing_set_descriptors[iddes][countRepre])
                    else:
                        repres_descs_temp[el].append(np.zeros(ldesc))
                repres_descs_temp[el] = np.array(repres_descs_temp[el])
            self.Kernel.repres_descs.append((repres_descs_temp))
        self.Kernel.nAt_repr = len(self.Kernel.representing_set_descriptors[0])
        
    def predictDepricated(self,predict_set,predict_system_charges=None):
        """Predicts dipole vectors, charges and electronegativities for list of atoms objects.
 
        Parameters
        ----------
            predict_set : listget_Abar
                List of atoms objects
            predict_system_charges : list
                List of integers representing charges of system (if None, charges are set up to be zero)

        Returns
        -------
            dipole: array
                1D array of dipole vector elements
            charge: array
                1D array of charges
            eneg: array
                1D array of electronegativities

        """ 
        print("Bulk prediction of qs, and dipoles started")
        if predict_system_charges == None:
            predict_system_charges = [0 for temp in predict_set]
        A, X, O, R, Xback = self._build_prediction(predict_set, predict_system_charges)
        Ks = self.Kernel.kernel_matrices(mol_set1 = predict_set,kerneltype="predicting")
        nAt = 0
        for mol in predict_set:
            nAt += len(mol)
        finDip = np.zeros(3*len(predict_set))
        finCharge = np.zeros(nAt)
        finEng = np.zeros(nAt)
        s,e = 0,0
        for desc in range(len(self.Kernel.Descriptors)):
            e = Ks[desc].shape[1]*(1+desc)
            eneg  = np.matmul(Ks[desc],self.weights[s:e])
            eneg_tot = (np.matmul(X,np.transpose(eneg)))
            eneg_tot = eneg_tot + O
            charge_temp = np.matmul(A,-eneg_tot)
            charge = np.matmul(Xback,charge_temp)
            dipole  = np.matmul(R,charge_temp)
            finDip += dipole
            finCharge += charge
            finEng += eneg
            s = e
        return finDip,finCharge,finEng

    def predict(self,predict_set,predict_system_charges=None):
        """Predicts dipole vectors, charges and electronegativities for list of atoms objects.
 
        Parameters
        ----------
            predict_set : listget_Abar
                List of atoms objects
            predict_system_charges : list
                List of integers representing charges of system (if None, charges are set up to be zero)

        Returns
        -------
            dipole: array
                1D array of dipole vector elements
            charge: array
                1D array of charges
            eneg: array
                1D array of electronegativities

        """ 
        print("Bulk prediction of qs, and dipoles started")
        if predict_system_charges == None:
            predict_system_charges = [0 for temp in predict_set]
        A, X, O, R, Xback = self._build_prediction(predict_set, predict_system_charges)
        Ks = self.Kernel.kernel_matrices(mol_set1 = predict_set,kerneltype="predicting")
        nAt = 0
        for mol in predict_set:
            nAt += len(mol)
        K = np.hstack(Ks)
        enegs = np.matmul(K,self.weights)
        eneg_tot = (np.matmul(X,np.transpose(enegs)))
        eneg_tot = eneg_tot + O
        charge_temp = np.matmul(A,-eneg_tot)
        charge = np.matmul(Xback,charge_temp)
        dipole  = np.matmul(R,charge_temp)
        return {'dipoles':dipole,'charges':charge}

    def calculate_bulk(self,predict_set,predict_system_charges=None):
        dKtrain = []
        Ktrain = []
        invAs = []
        As = []
        dAs = []
        nAts = []
        for mol in predict_set:
            nAts.append(len(mol))
        max_nAts = np.max(nAts)
        n_w = len(self.weights)
        for id_A in range(len(nAts)):
            Ks,dKdrs  = self.Kernel._calculate_function(mol_set1 = predict_set[id_A])
            # print(np.array(Ks).shape)
            dKdrs = np.array(dKdrs)
            Ks = np.array(Ks)
            hold_tensor = np.zeros([Ks.shape[0],max_nAts,Ks.shape[2]])
            hold_tensor[:,:Ks.shape[1],:] += Ks
            Ktrain.append(hold_tensor)
            hold_tensor = np.zeros([dKdrs.shape[0],max_nAts,3,max_nAts,dKdrs.shape[-1]])
            # print(dKdrs.shape,hold_tensor.shape)
            
            hold_tensor[:,:dKdrs.shape[1],:,:dKdrs.shape[3],:] += dKdrs
            dKtrain.append(hold_tensor)
            qe = charge_eq(predict_set[id_A],Qtot=0,scale_atsize=self.scale_atsize,DoLJ=self.LJ,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            invA = qe.get_Ainv()
            A = qe.get_A()
            dA = qe.get_dAdr()
            hold_tensor = np.zeros([max_nAts,max_nAts,3])
            hold_tensor[:dA.shape[0],:dA.shape[1],:] += dA
            dAs.append(hold_tensor)
            hold_matrix = np.zeros((max_nAts+1,max_nAts+1))
            hold_matrix[:invA.shape[0],:invA.shape[1]] += invA
            invAs.append(hold_matrix)#[:invA.shape[0],:invA.shape[1]] + invA)
            hold_matrix = np.zeros((max_nAts,max_nAts))
            hold_matrix[:A.shape[0],:A.shape[1]] += A
            As.append(hold_matrix)#[:As.shape[0],:As.shape[1]] + As)
        invAs = np.array(invAs)
        As = np.array(As)
        dAs = np.array(dAs)
        Ktrain = np.array(Ktrain)
        dKtrain = np.array(dKtrain)
        enegs = np.einsum("pknm,m->pn",Ktrain,self.weights)
        enegs_ap = np.zeros((enegs.shape[0],enegs.shape[1]+1))
        enegs_ap[:,:enegs.shape[-1]] += enegs
        charges = -np.einsum("pni,pi->pn",invAs,enegs_ap)[:,:-1]
        energy = np.einsum("pn,pn->p",charges,enegs)+0.5*np.einsum("pn,pni,pi->p",charges,As,charges)
        enegs_drj = np.einsum("pkabcd,d->pabc",dKtrain,self.weights)
        f_eneg = -np.einsum("pidj,pj->pid",enegs_drj,charges)
        f_qeq = -1*np.einsum("pi,pj,pijd->pid",charges,charges,dAs)
        forces = (Hartree/Bohr) * (f_eneg + f_qeq)
        ret_qs = []
        ret_fs = []
        for id_a,a in enumerate(nAts):
            ret_fs.extend(forces[id_a][:a])
            ret_qs.extend(charges[id_a][:a]) 
        results = {'charges':ret_qs,'energy':energy*Hartree,'forces':ret_fs}
        return results
        # dAs = np.array(dAs)



    def _build_predictions(self,mols,systems_charges,nAtoms = None):
        all_rs = []
        if nAtoms == None:
            nAtoms = 0
            for mol in mols:
                nAtoms += len(mol)
        count_row = 0
        count_col = 0
        count_rowBack = 0
        count_colBack = 0
        X = np.zeros((nAtoms + len(mols),nAtoms))
        Xback = np.zeros((nAtoms,nAtoms+len(mols)))
        O = np.zeros((nAtoms + len(mols)))
        countChar = 0
        dim = 0
        for mol in mols:
            all_rs.append(_get_R(mol))
            dim += len(mol)+1
            for at in mol:
                Xback[count_rowBack,count_colBack] = 1
                X[count_row,count_col] = 1
                count_row += 1
                count_col += 1
                count_colBack += 1
                count_rowBack += 1
            O[count_row] = -systems_charges[countChar]
            countChar += 1
            count_row +=1
            count_colBack += 1
        R = _block_diag_rect(all_rs,dim,len(all_rs)*3)
        return X, O, R, Xback


    
    def _build_prediction(self,mols,systems_charges,nAtoms = None):
        """Computes blocked matrixes needed for the predict function

        Parameters
        ----------
            predict_set : list
                List of atoms objects
            predict_system_charges : list
                List of integers representing charges of system
            nAtoms : int
                Number of atoms in the prediction set
        
        Returns
        -------
            A : array
                Blocked inverse hardness matrix
            X : array
                Transformation matrix for N+1 vector 
            O : array
                Vector of zeroes and -Q of the system
            R : array
                Blocked dipole transformation matrix
            Xback : array
                Transformation matrix for creating N vector
        """   

        all_rs = []
        if nAtoms == None:
            nAtoms = 0
            for mol in mols:
                nAtoms += len(mol)
        count_row = 0
        count_col = 0
        count_rowBack = 0
        count_colBack = 0
        X = np.zeros((nAtoms + len(mols),nAtoms))
        Xback = np.zeros((nAtoms,nAtoms+len(mols)))
        O = np.zeros((nAtoms + len(mols)))
        countChar = 0
        dim = 0
        all_as = []
        for id_mol,mol in enumerate(mols):
            all_rs.append(_get_R(mol))
            qe   = charge_eq(mol,scale_atsize=self.scale_atsize,Qtot=systems_charges[id_mol],radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            dim += qe.nAt+1
            all_as.append(qe.get_Ainv())
            for at in mol:
                Xback[count_rowBack,count_colBack] = 1
                X[count_row,count_col] = 1
                count_row += 1
                count_col += 1
                count_colBack += 1
                count_rowBack += 1
            O[count_row] = -systems_charges[countChar]
            countChar += 1
            count_row +=1
            count_colBack += 1
        A = _block_diag(all_as,dim)
        R = _block_diag_rectR(all_rs,dim,len(all_rs)*3)
        return A, X, O, R, Xback



    def _process_ref_energy(self, charges):
        Qmatrix = np.zeros((self.Kernel.nAt_train,len(self.Kernel.training_set)))
        Cs = []
        count_c = 0
        count_r = 0
        for ids,mol in enumerate(self.Kernel.training_set):
            q_temp = []
            for q in mol: 
                Qmatrix[count_r,count_c] = charges[count_r]
                q_temp.append(charges[count_r])
                count_r += 1
            count_c += 1
        return Qmatrix
        # return Cs, Qmatrix



    def loss_energy(self,params):
        weights = params
#        print(weights[:100])
        mols = self.Kernel.training_set
        train_energy = []
        for id_mol in range(len(mols)):
            dKdrs = self.dKtrain[id_mol]
            Ks = self.Ktrain[id_mol]
            KsStuck = np.hstack(Ks)#/Bohr
            # dKdrs = np.array(dKdrs)
            eneg = np.matmul(KsStuck,weights)#/Hartree
            qe = charge_eq(mols[id_mol],Qtot=0,eneg=eneg,scale_atsize=self.scale_atsize,DoLJ=self.LJ,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            qe.calc_Charges()
            qe.calc_Eqeq()
            train_energy.append(qe.E*Hartree)
        train_energy = np.array(train_energy)#.flatten()
        # ref = 1*get_forces_eV(mols=self.Kernel.training_set, forces_keyword = "forces").flatten()
        atom_energy = {"H": -12.6746244439181, "O": -2041.03979050724}
        ref = get_energies_eV(mols=mols,atom_energy = atom_energy)
        mse = (ref - train_energy)@(ref - train_energy)
        print(ref[:30])
        print(train_energy[:30])
        print("mse:",mse)
        return mse

    def loss_force(self,params):
        weights = params
#        print(weights[:100])
        mols = self.Kernel.training_set
        train_forces = []
        for id_mol in range(len(mols)):
            dKdrs = self.dKtrain[id_mol]
            Ks = self.Ktrain[id_mol]
            KsStuck = np.hstack(Ks)#/Bohr
            # dKdrs = np.array(dKdrs)
            eneg = np.matmul(KsStuck,weights)#/Hartree
            qe = charge_eq(mols[id_mol],Qtot=0,eneg=eneg,scale_atsize=self.scale_atsize,DoLJ=self.LJ,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            qe.calc_Charges()
            qe.calc_Fqeq()
            charges  = qe.q
            f_qeq  = qe.f
            enegs_drj = np.einsum("abcd,d->abc",dKdrs.squeeze(),weights)
            charges_enegs_drj = np.einsum("k,ijk->ijk",charges,enegs_drj)
            f_eneg = -np.sum(charges_enegs_drj, axis=2)
            forces = (Hartree/Bohr) * (f_eneg + f_qeq)
            train_forces.extend(forces)
        train_forces = np.array(train_forces).flatten()
        ref = -1*get_forces_eV(mols=self.Kernel.training_set, forces_keyword = "forces").flatten()
        mse = (ref - train_forces)@(ref - train_forces)
        # print(ref[:30])
        # print(train_forces[:30])
        # print("mse:",mse)
        return mse


    def train(self, 
              targets = ["dipole"], 
              target_sigmas = [1], 
              max_iter = 50,
              atom_energy=None,
              n_bad_iters = 5,
              scc_target=False,
              scc_sigma=0.1,
              charge_keyword=None,
              dipole_keyword=None,
              energy_keyword="energy",
              forces_keyword="forces",
              verbose=True): # print("hello seaman")
        '''
        Training is done by solving QR problem. 
        '''
        train_nAts_array = []
        for mol in self.Kernel.training_set:
            train_nAts_array.append(len(mol))
        ones_for_Qmatrix = np.array(create_L_matrix(train_nAts_array))
        oldRMSE = np.inf
        # print(oldRMSE)
        if atom_energy == None:
            atom_energy = {a:0 for a in self.elements}
            E_ref = [0.0 for a in self.Kernel.training_set]
        else:
            E_ref = get_energies_atomic(mols=self.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
        noniterative_targets_templates = ["charges","dipole"]
        iterative_targets_templates = ["energy","forces"]
        noniterative_targets_current = []
        noniterative_targets_current_sigmas = []
        iterative_targets_current = []
        iterative_targets_current_sigmas = []

        for id_t in range(len(targets)):
            if targets[id_t] in noniterative_targets_templates: 
                noniterative_targets_current.append(targets[id_t])
                noniterative_targets_current_sigmas.append(target_sigmas[id_t])
            if targets[id_t] in iterative_targets_templates: 
                iterative_targets_current.append(targets[id_t])
                iterative_targets_current_sigmas.append(target_sigmas[id_t])
        if len(iterative_targets_current) == 0:
            max_iter = 0
        print("iterative_targets_current",iterative_targets_current,iterative_targets_current_sigmas)
        print("noniterative_targets_current",noniterative_targets_current,noniterative_targets_current_sigmas)


        bigKnm = np.hstack(self.K_nms)
        bigKmm = stack_matrices_diagonally(self.K_mms)
        up = np.zeros(bigKmm.shape[1])
        down = np.zeros((bigKmm.shape[1],bigKmm.shape[1]))
        if charge_keyword != None:
            q_ref_iterative = get_charges(self.Kernel.training_set,charge_keyword=charge_keyword)
        if len(noniterative_targets_current) > 0:
            L, b_plus, ref = self._process_data_noniterative(noniterative_targets_current[0],charge_keyword,dipole_keyword)
            diag_sigmas_sqrt = np.sqrt((1/noniterative_targets_current_sigmas[0]))*np.ones(len(ref))
            b_up = diag_sigmas_sqrt*(ref + b_plus)
            B_up = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
            for id_target in range(1,len(noniterative_targets_current)):
                L, b_plus,ref = self._process_data_noniterative(noniterative_targets_current[id_target],charge_keyword,dipole_keyword)
                diag_sigmas_sqrt = np.sqrt((1/noniterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                b_up = np.concatenate((b_up,(ref + b_plus)*diag_sigmas_sqrt))
                B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                B_up = np.concatenate((B_up,B_up_temp),axis=1)

            noniterative_B_up = B_up
            noniterative_b_up = b_up
        training_on = True
        count_iter = 0

        # O = self.O_train
        # X = self.X_train
        bad = 1
        scc_training = False
        if max_iter > 0:
            Xback = self._get_Xback()
            A,Abar,Ainv = self._build_As(self.Kernel.training_set)
            _,X, O = self._build_A_X_O(mols=self.Kernel.training_set,systems_charges=self.Kernel.training_system_charges)
        while training_on:
            if verbose == True:
                print("####################################################################")
            for id_target in range(0,len(iterative_targets_current)):
                if iterative_targets_current[id_target] == "energy":
                    L, b_plus,ref = self._process_data_iterative(iterative_targets_current[id_target],q_ref_iterative,atom_energy,energy_keyword,forces_keyword)
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    try:
                        b_up = np.concatenate((b_up,(-ref + b_plus)*diag_sigmas_sqrt))
                        B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                        B_up = np.concatenate((B_up,B_up_temp),axis=1)
                    except:
                        b_up = diag_sigmas_sqrt*(-ref + b_plus) 
                        B_up = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                elif iterative_targets_current[id_target] == "forces":
                    allKLsforces = None
                    current_position_for_force = 0
                    qeq_forces = []
                    for mol in self.Kernel.training_set:
                        current_charges = q_ref_iterative[current_position_for_force:current_position_for_force+len(mol)]                         
                        current_position_for_force += len(mol)
                        Ks,dKdrs  = self.Kernel._calculate_function(mol_set1 = mol)
                        qe = charge_eq(mol,Qtot=0,q=current_charges,scale_atsize=self.scale_atsize,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
                        # qe.calc_Charges() 
                        qe.calc_Fqeq()
                        qeq_forces.extend((qe.f).flatten())#*(Hartree/Bohr))
                        dKdrs = np.array(dKdrs)
                        current_LK = np.einsum("kadAe,A->kade",dKdrs,current_charges) #k - number of kernels, a - soap derivatives of these atom, d - direction (3), A - wrt to this charges, e - number of weigts
                        current_LK = current_LK.reshape(-1,dKdrs.shape[-1])
                        try:
                            allKLsforces = np.concatenate((allKLsforces,current_LK))
                        except:
                            allKLsforces = current_LK

                    ref = -1*get_forces_atomic(mols=self.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    b_up_temp = diag_sigmas_sqrt*(-ref - np.array(qeq_forces))
                    B_up_temp = diag_sigmas_sqrt*allKLsforces.T
                    # print("forces test",B_up_temp.shape,b_up_temp.shape,diag_sigmas_sqrt.shape,allKLsforces.shape)
                    try:
                        b_up = np.concatenate((b_up,b_up_temp))
                        # B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                        B_up = np.concatenate((B_up,B_up_temp),axis=1)
                    except:
                        b_up = b_up_temp 
                        B_up = B_up_temp #diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])

            try:
                B_up = np.concatenate((noniterative_B_up,B_up),axis=1)
                b_up = np.concatenate((noniterative_b_up,b_up))
            except:
                B_up = B_up
                b_up = b_up
            if scc_training:
                L, b_plus,ref = self._process_data_iterative("scc",q_ref_iterative,atom_energy,energy_keyword,forces_keyword)
                diag_sigmas_sqrt = np.sqrt((1/scc_sigma))*np.ones(len(ref))
                b_up = np.concatenate((b_up,(ref + b_plus)*diag_sigmas_sqrt))
                B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                B_up = np.concatenate((B_up,B_up_temp),axis=1)
                scc_sigma = scc_sigma*0.5
            Umm = np.linalg.cholesky(bigKmm).T
            B=np.concatenate((B_up.T,Umm))
            b = np.concatenate((b_up,np.zeros(bigKmm.shape[0])))
            Q,R = np.linalg.qr(B) #,mode="complete")
            Qb = np.dot(Q.T,b)
            weights = -np.linalg.solve(R,Qb)
            if max_iter > 0:

                eneg = np.matmul(bigKnm,weights)#/Hartree
                eneg_tot = (np.matmul(X,np.transpose(eneg)))
                eneg_tot = eneg_tot + O
                charge_temp = np.matmul(Ainv,-eneg_tot)
                q_new = np.matmul(Xback,charge_temp)
                Qmatrix_temp = ones_for_Qmatrix*q_new
                Cs_temp = 0.5*Qmatrix_temp@Abar@Qmatrix_temp.T
                Cs_temp = Cs_temp.sum(axis=0)
                whole_energy =  Cs_temp + np.linalg.multi_dot([Qmatrix_temp,eneg])
                MSE = np.square(np.subtract(E_ref,whole_energy)).mean()
                RMSE_E = np.sqrt(MSE)

                B_up = None
                b_up = None
                B = None
                b = None

                ###########################
                if verbose == True:
                    print("New RMSE:",RMSE_E)
                    print("Old RMSE:",oldRMSE)
                    print("Bad RMSEs in row:", bad)
                    print("Sample of charges:",q_new[-10:])
                    print("sum of charges: ",np.sum(q_new))
                    print(f"iteration {count_iter} is done")
                    print("####################################################################")
                if (RMSE_E > oldRMSE and bad >= n_bad_iters) or (count_iter == max_iter):
                    training_on = False
                    # final_weight = weights
                elif RMSE_E > oldRMSE and bad < n_bad_iters and scc_training == False:
                    bad += 1
                    q_ref_iterative = q_new
                    # final_weight = weights # delete this later
                    if scc_target == True:
                        print("Activating scc")
                        scc_training = True
                elif RMSE_E > oldRMSE and bad < n_bad_iters and scc_training == True:
                    bad += 1
                    q_ref_iterative = q_new
                else:
                    print("New best model reached")
                    bad = 1
                    final_weight = weights
                    oldRMSE = RMSE_E
                    q_ref_iterative = q_new
                count_iter += 1
            if max_iter == 0:
                training_on = False
                final_weight = weights
        print("Lenght of weights:",len(final_weight))
        self.weights = final_weight

    # def _calc_res_per_mol(self):

        # return Edep,Eind,enegs,qs


    def _train_up_down(self,smallT,ref,sigma_reg,K_nm):
        print("Training on electrostatic properties subprocess started")
        # X = self.prepMatric(self.X_train)
        O = self.O_train
        A = self.A_train
        X = self.X_train
        T = smallT.T
        Sigma = ((1/sigma_reg)*np.eye(len(ref)))
        up1 = np.linalg.multi_dot([K_nm.T,X.T,A.T,T,Sigma,ref])
        up2 = np.linalg.multi_dot([K_nm.T,X.T,A.T,T,Sigma,T.T,A,O])
        up = up1+up2
        down = np.linalg.multi_dot([K_nm.T,X.T,A.T,T,Sigma,T.T,A,X,K_nm])
        return up, down

    

    def _kernel_forces(self,mol,q,deriv="numerical"):
        dKdr = self.Kernel._an_kernel_gradient(mol)
        #dKdr = self.Kernel._num_kernel_gradient(mol)
        nAt  = len(q)
        f_k  = np.zeros((nAt,3))
        for j in range(nAt):
            for direction in range(3):
                eneg_drj = np.matmul(dKdr[j][direction],self.weights)
                for i in range(nAt):
                    f_k[j,direction] += -q[i]*eneg_drj[i]
        return f_k

    def _build_As(self,mols):
        all_asInv = []
        all_as = []
        all_asbars = []
        dim = 0
        dim1 = 0
        for mol in mols:
            qe   = charge_eq(mol,scale_atsize=self.scale_atsize,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            dim1 += qe.nAt+1
            dim += qe.nAt
            A_temp = qe.get_A()
            all_asbars.append(A_temp)
            A_temp = np.row_stack((A_temp, np.ones(qe.nAt)))
            A_temp = np.column_stack((A_temp, np.ones(qe.nAt+1)))
            A_temp[-1,-1] = 0
            all_as.append(A_temp)
            Ainv = np.linalg.inv(A_temp)            
            all_asInv.append(Ainv)
            
        AInv = _block_diag(all_asInv,dim1)
        Abar =_block_diag(all_asbars,dim)
        A =_block_diag(all_as,dim1)
        return A,Abar,AInv

    

    def _build_dAs(self,mols):
        all_asInv = []
        all_as = []
        all_asbars = []
        dim = 0
        dim1 = 0
        for mol in mols:
            qe   = charge_eq(mol,scale_atsize=self.scale_atsize,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            dim1 += qe.nAt+1
            dim += qe.nAt
            A_temp = qe.get_dA()
            all_asbars.append(A_temp)
            
        dAs =_block_diag(all_asbars,dim)
        return dAs


    def _build_A(self,mols):
        all_asInv = []
        all_as = []
        dim = 0
        for mol in mols:
            qe   = charge_eq(mol,scale_atsize=self.scale_atsize,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            dim += qe.nAt
            A_temp = qe.get_A()
            all_as.append(A_temp)
            A_temp = np.row_stack((A_temp, np.ones(qe.nAt)))
            A_temp = np.column_stack((A_temp, np.ones(qe.nAt+1)))
            A_temp[-1,-1] = 0
            Ainv = np.linalg.inv(A_temp)            
            all_asInv.append(Ainv[:-1,:-1])
            
        AInv = _block_diag(all_asInv,dim)
        A =_block_diag(all_as,dim)
        return A,AInv

    def _build_A_X_O(self,mols=None,systems_charges=None,nAtoms = None):
        """Computes blocked matrixes needed for predict function

        Parameters
        ----------
            predict_set : list
                List of atoms objects
            predict_system_charges : list
                List of integers representing charges of system
            nAtoms : int
                Number of atoms in the prediction set
        
        Returns
        -------
            A : array
                Blocked inverse hardness matrix
            X : array
                Transformation matrix for N+1 vector 
            O : array
                Vector of zeroes and -Q of the system
            R : array
                Blocked dipole transformation matrix
            Xback : array
                Transformation matrix for creating N vector
        """   

        if nAtoms == None:
            nAtoms = 0
            for mol in mols:
                nAtoms += len(mol)
        count_row = 0
        count_col = 0
        X = np.zeros((nAtoms + len(mols),nAtoms))
        O = np.zeros((nAtoms + len(mols)))
        countChar = 0
        dim = 0
        all_as = []
        for id_mol,mol in enumerate(mols):
            qe   = charge_eq(mol,scale_atsize=self.scale_atsize,Qtot=systems_charges[id_mol],radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
            dim += qe.nAt+1
            all_as.append(qe.get_Ainv())
            for at in mol:
                X[count_row,count_col] = 1
                count_row += 1
                count_col += 1
            O[count_row] = -systems_charges[countChar]
            countChar += 1
            count_row +=1
        A = _block_diag(all_as,dim)
        return A, X, O

    def _get_Xback(self):
        mols = self.Kernel.training_set
        nAtoms = 0
        for m in mols:
            nAtoms += len(m)
        count_colBack = 0
        count_rowBack = 0
        Xback = np.zeros((nAtoms,nAtoms+len(mols)))
        for mol in mols:
            for at in mol:
                Xback[count_rowBack,count_colBack] = 1
                count_colBack += 1
                count_rowBack += 1
            count_colBack += 1
        return Xback

    def _build_A_X_Osingle(self,mol=None,Qtot=None):
        nAtoms = len(mol)
        X = np.zeros((nAtoms + 1,nAtoms))
        O = np.zeros((nAtoms + 1))
        dim = 0
        qe   = charge_eq(mol,scale_atsize=self.scale_atsize,Qtot=Qtot,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
        dim += qe.nAt+1
        A = qe.get_Ainv()
        for at in range(len(mol)):
            X[at,at] = 1
        O[-1] = -Qtot

        return A, X, O

    def _process_data_noniterative(self,target,charge_keyword,dipole_keyword):
        mols = self.Kernel.training_set
        if target == "charges":
            O = self.O_train
            A = self.A_train
            X = self.X_train
            ref_qs = []
            nAtoms = 0
            for m in mols:
                nAtoms += len(m)
            count_rowBack = 0
            count_colBack = 0
            Xback = np.zeros((nAtoms,nAtoms+len(mols)))
            for mol in mols:
                ref_qs.extend(mol.arrays[charge_keyword])
                for at in mol:
                    Xback[count_rowBack,count_colBack] = 1
                    count_colBack += 1
                    count_rowBack += 1
                count_colBack += 1
                refdata = np.array(ref_qs)
            L = np.linalg.multi_dot([X.T,A.T,Xback.T])
            b_plus = np.linalg.multi_dot([Xback,A,O])
            print("L ", L.shape, "b_plus ", b_plus.shape, "ref",refdata.shape) 
            return L, b_plus, refdata
        
        elif target == "dipole":
            # O = self.O_train
            # A = self.A_train
            # X = self.X_train
            all_rs = []
            ref_mus = []
            dimR = 0
            b_all = []
            B_all = []
            dimB1 = 0
            dimB2 = 0
            for mol in mols:
                A, X, O = self._build_A_X_Osingle(mol=mol,Qtot=0)
                ref_mus.extend(mol.info[dipole_keyword])
                R = _get_R(mol) 
                R = np.hstack((R, np.zeros((3, 1))))
                B_temp = np.linalg.multi_dot([X.T,A.T,R.T])
                b_temp = np.linalg.multi_dot([R,A,O])
                dimB1 += B_temp.shape[0]
                dimB2 += B_temp.shape[1]
                b_all.append(b_temp)
                B_all.append(B_temp.T)
            refdata = np.array(ref_mus)
            L = _block_diag_rect(B_all,dimB1,dimB2).T
            b_plus = np.concatenate(b_all)
            return L, b_plus,refdata

    def _process_data_iterative(self,target, charges,atom_energy=None,energy_keyword=None,forces_keyword=None):
        mols = self.Kernel.training_set
        if target == "energy":
            # refdata = -1*get_energies_atomic(mols=self.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
            refdata = get_energies_atomic(mols=self.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
            Qmatrix = np.zeros((self.Kernel.nAt_train,len(self.Kernel.training_set)))
            Cs = []
            count_c = 0
            count_r = 0
            A,Abar,Ainv = self._build_As(self.Kernel.training_set)
            for ids,mol in enumerate(self.Kernel.training_set):
                q_temp = []
                for q in mol:
                    Qmatrix[count_r,count_c] = charges[count_r]
                    q_temp.append(charges[count_r])
                    count_r += 1
                count_c += 1
            Cs = 0.5*Qmatrix.T@Abar@Qmatrix
            Cs = Cs.sum(axis=0)#*Hartree
            return Qmatrix, Cs, refdata # Qmatrix is L matrix in GAP, but instead of 1s there are charges
        elif target == "scc":
            O = self.O_train
            A = self.A_train
            X = self.X_train
            ref_qs = charges
            nAtoms = 0
            for m in mols:
                nAtoms += len(m)
            count_rowBack = 0
            count_colBack = 0
            Xback = np.zeros((nAtoms,nAtoms+len(mols)))
            for mol in mols:
                for at in mol:
                    Xback[count_rowBack,count_colBack] = 1
                    count_colBack += 1
                    count_rowBack += 1
                count_colBack += 1
                refdata = np.array(ref_qs)
            L = np.linalg.multi_dot([X.T,A.T,Xback.T])
            b_plus = np.linalg.multi_dot([Xback,A,O])
            return L, b_plus, refdata
            # print("#################CS")
            # print(Cs)
            # print(refdata)
            # print("##################CS")
    
    def TrainEnergy(self,weights,A,Ainv,Lgap):
        Knm = np.hstack(self.K_nms)
        enegs = np.matmul(Knm,weights)
        charges = np.matmul(Ainv,-enegs)
        Qmatrix = Lgap*charges[:,np.newaxis]
        Cs_temp = 0.5*Qmatrix.T@A@Qmatrix
        Cs = Cs_temp.sum(axis=0)
        EkQEq =  Cs + np.linalg.multi_dot([Qmatrix.T,enegs])
        return EkQEq,Cs, Qmatrix,charges


    def _farthest_point(self,K_matrix,n_struc,seeds):
        """Farthest point sampling algorithm for sparsification. At this point, this is depricated

        Parameters
        ----------
            K_matrix : array 
                Kernel matrix
            n_struc : int
                Number of structures for representative set
        
        Returns
        -------
            samples : array
                Representative set
        """   

        dia = K_matrix.diagonal()
        dist_matrix_squared = dia[:, None] + dia[None, :] - 2*K_matrix
        control_value = -1e-8
        control = np.nonzero(dist_matrix_squared < control_value)
        assert len(control[0]) == 0, f'Squared distance matrix element < {control_value}\n  \
                    Indices are : {np.argwhere(dist_matrix_squared<control_value)}.\n  \
                    Please check your code something went wrong!'

        dist_matrix_squared[control] = 0
        dist_matrix = np.sqrt(dist_matrix_squared)
        if isinstance(seeds, int):
            israise = True if n_struc <= 1 else False

            samples = [seeds]  # storage for furthest samples
            samples.append(np.argmax(dist_matrix[seeds]))  # find furthest from input sample

        elif isinstance(seeds, (list, np.ndarray)):
            israise = True if n_struc <= len(seeds) else False

            samples = [sample for sample in seeds]

        if israise:
            raise ValueError('`number` can not be smaller than specied in `seeds`')


        for idx in range(n_struc-len(samples)):
            samples_rem = np.delete(np.arange(len(dist_matrix)), samples)  # get indices of no selected samples

            dists = dist_matrix[samples][:, samples_rem]  # slice distances for selected samples to remaining samples

            dists_min = np.min(dists, axis=0)  # for each remaining sample find closest distance to already selected sample
            sample_furthest = np.argmax(dists_min)  # select the remaining sample furthest to all selected samples

            samples.append(samples_rem[sample_furthest])
        return samples

    def give_qeq(self,mol,charge=0):
        A, X, O = self._build_A_X_O(mols=[mol],systems_charges=[charge])
        Ks = self.Kernel.kernel_matrices(mol_set1 = [mol],kerneltype="predicting")
        nAt = len(mol)
        enegs = np.matmul(Ks,self.weights)
        eneg_tot = (np.matmul(X,np.transpose(enegs)))
        eneg_tot = eneg_tot + O
        charges = np.matmul(A,-eneg_tot)[:-1]
        KsStuck = np.hstack(Ks)#/Bohr
        eneg = np.matmul(KsStuck,self.weights)#/Hartree
        qe     = charge_eq(mol,Qtot=charge,eneg=eneg,scale_atsize=self.scale_atsize,DoLJ=self.LJ,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
        return qe 
