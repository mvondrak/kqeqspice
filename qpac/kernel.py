import numpy as np
from ase.io import *
from ase.data import covalent_radii
from ase.units import Bohr
from qpac.utils import CURdecomposition
import random
# from quippy.descriptors import Descriptor
from dscribe.descriptors import SOAP
np.random.seed(10)

import time

class qpacKernel():
    def __init__(self,
                 Descriptors=[],
                 training_set=None, 
                 training_set_charges = None,
                 perEl = True, 
                 sparse=False, 
                 sparse_count=None, 
                 sparseSelect = None,
                 sparse_method="CUR",
                 zeta=2):
        if type(Descriptors) != list:
            Descriptors = [Descriptors]
        #    exit()
        elif len(Descriptors) == 0:
            print("Specify descriptors")
            exit()
        self.zeta = zeta        
        self.Descriptors = Descriptors
        self.elements = self.Descriptors[0].species
        self.sparse = sparse
        self.sparse_count = sparse_count
        self.perEl = perEl
        self.training_set_descriptors = []
        self.training_set_elements = []
        self.nAt_train = 0
        
        if training_set is not None:
            if training_set_charges == None:
                self.training_system_charges = [0 for temp in training_set] 
            else: 
                self.training_system_charges = training_set_charges    
            for mol in training_set:
                self.nAt_train += len(mol)
            self.training_set = training_set
            for iddesc, desc in enumerate(self.Descriptors):
                print(f"Computing training descriptors for for descriptor number {iddesc}")
                desc.compute(self.training_set,"training")
                self.training_set_descriptors.append(desc.precomputed["training"])
                self.training_set_elements.append(desc.precomputedEl["training"])
        else:
            for desc in self.Descriptors:
                print("Training set is not set up, this object can be used only for production runs")
                try:
                    self.training_set_descriptors.append(None)
                    self.training_set.append(None)
                    self.training_system_charges.append(None)
                    self.training_set_elements.append(None)
                except:
                    self.training_set_descriptors = [None]
                    self.training_set = [None]
                    self.training_system_charges = [None]
                    self.training_set_elements = [None]
        if self.sparse == True and training_set is not None:
            self.sparse_method = sparse_method
            # TODO : different sparse for different descriptors/kernels
            self.sparse_count = int(sparse_count)
            if sparse_method == "CUR":
                print("Starting CUR decomposition for each element (final number of sparse points will be number of species * sparse_count)")
                self.sparse_points = self._create_representationCUR_elements(self.sparse_count)
                self.nAt_repr = len(self.representing_set_descriptors[0])
            elif sparse_method == "prepicked":
                self.sparse_points = sparseSelect
                self.representing_set_descriptors = []
                self.representing_set_elements = []
                for iddesc in range(len(self.Descriptors)):
                    self.representing_set_descriptors.append(np.load(f"KQEqSOAP_{iddesc}.npy"))
                    self.representing_set_elements.append(np.load(f"KQEqEl_{iddesc}.npy"))
            elif sparse_method == "random":
                print("Picking sparse points randomly")
                # self.sparse_points = random.sample(range(0,self.nAt_train),self.sparse)
                self.sparse_points = self._create_random_picked_elements(self.sparse_count)
                self.nAt_repr = len(self.representing_set_descriptors[0])
                # self._create_representationPrepicked(nameS="KQEqSOAP.npy",nameEl="KQEqEl.npy")
            elif sparse_method == None:
                pass    
            else:
                print("Specify sparse method!")
                exit()
        # TODO : write the code for the sparse == False
        #elif self.sparse == False and training_set is not None        :
        #    self.representing_set_descriptors = self.training_set_descriptors
        #    self.representing_set_elements = self.training_set_elements
        
        #######################################################################
        if self.training_set[0] is not None:
            self.train_descs = []
            self.repres_descs = []
            for iddesc in range(len(Descriptors)):
                ldesc = len(self.training_set_descriptors[iddesc][0])
                el_train = {}
                el_repr = {}
                for el in self.elements:
                    el_repr[el] = []
                    #if self.perEl == True:
                    # TODO : this is now written only of self.perEl == True. This has to be changed, there are more places where this is missing
                    el_train[el] = []
                    for countTrain in range(len(self.training_set_descriptors[iddesc])):
                        if el == self.training_set_elements[iddesc][countTrain]:
                            el_train[el].append(self.training_set_descriptors[iddesc][countTrain])
                        else:
                            el_train[el].append(np.zeros(ldesc))
                    for countRepre in range(len(self.representing_set_descriptors[iddesc])):
                        if el == self.representing_set_elements[iddesc][countRepre]:
                            el_repr[el].append(self.representing_set_descriptors[iddesc][countRepre])
                        else:
                            el_repr[el].append(np.zeros(ldesc))
                    el_train[el] = np.array(el_train[el])
                    el_repr[el] = np.array(el_repr[el])
                self.train_descs.append(el_train)
                self.repres_descs.append(el_repr)
                #self.repres_descs = np.array(self.repres_descs)
                # np.save("repres_descs.npy",self.repres_descs,allow_pickle=True)
        #######################################################################

    def _create_representationCUR_elements(self,sparse_count):
        pickedAll = []
        self.representing_set_descriptors = []
        self.representing_set_elements = []
        for iddesc in range(len(self.Descriptors)):
            specs = []
            for mol in self.training_set:
                for atom in mol:
                    specs.append(atom.symbol)
            soapVecs = {a:[] for a in self.Descriptors[iddesc].species}
            soapIds = {a:[] for a in self.Descriptors[iddesc].species}
            for id, el in enumerate(specs):
                soapVecs[el].append(self.training_set_descriptors[iddesc][id])
                soapIds[el].append(id)
            temp_rep = []
            temp_el = []
            pickedAlltemp = []
            for element in self.Descriptors[iddesc].species:
                if len(soapVecs[element]) > 0:
                    picked = CURdecomposition(sparse_count=sparse_count,mat=soapVecs[element])
                    temp_rep.extend([soapVecs[element][pos] for pos in picked]) 
                    temp_el.extend([element for pos in picked])
                    pickedAlltemp.extend([soapIds[element][pos] for pos in picked])
                else:
                    pickedAlltemp.exnted([])
            pickedAll.append(pickedAlltemp)
            self.representing_set_descriptors.append(np.array(temp_rep))
            self.representing_set_elements.append(temp_el)
        self.sparse = True
        #self.representing_set_descriptors = np.array(self.representing_set_descriptors)
        #self.representing_set_elements = np.array(self.representing_set_elements)

        return pickedAll

    def _create_random_picked_elements(self,sparse_count):
        pickedAll = []
        self.representing_set_descriptors = []
        self.representing_set_elements = []
        for iddesc in range(len(self.Descriptors)):
            specs = []
            for mol in self.training_set:
                for atom in mol:
                    specs.append(atom.symbol)
            soapVecs = {a:[] for a in self.Descriptors[iddesc].species}
            soapIds = {a:[] for a in self.Descriptors[iddesc].species}
            for id, el in enumerate(specs):
                soapVecs[el].append(self.training_set_descriptors[iddesc][id])
                soapIds[el].append(id)
            temp_rep = []
            temp_el = []
            pickedAlltemp = []
            for element in self.Descriptors[iddesc].species:
                if len(soapVecs[element]) > 0:
                    # print(len(soapVecs[element]), sparse_count)
                    if len(soapVecs[element]) >= sparse_count:
                        picked = random.sample(range(0,sparse_count),sparse_count)
                    else:
                        picked = random.sample(range(0,len(soapVecs[element])),len(soapVecs[element]))
                    # picked = CURdecomposition(sparse_count=sparse_count,mat=soapVecs[element])
                    temp_rep.extend([soapVecs[element][pos] for pos in picked]) 
                    temp_el.extend([element for pos in picked])
                    pickedAlltemp.extend([soapIds[element][pos] for pos in picked])
                else:
                    # If the element is not in the training set, but was asked for, create empty list for sparse points
                    pickedAlltemp.exnted([])
            pickedAll.append(pickedAlltemp)
            self.representing_set_descriptors.append(np.array(temp_rep))
            self.representing_set_elements.append(temp_el)
        self.sparse = True
        #self.representing_set_descriptors = np.array(self.representing_set_descriptors)
        #self.representing_set_elements = np.array(self.representing_set_elements)

        return pickedAll

    def _calculate_function(self,mol_set1):
        # print(self.representing_set_descriptors[0].shape)
        # print(self.Descriptors[0].calc_descriptor_derivative(mol_set1).shape)
        alldK = []
        allK = []
        for iddesc in range(len(self.Descriptors)):
            desc1,der1,nl1,elems1 = self.Descriptors[iddesc].calc_descriptor_derivative(mol_set1)#,deriv="numerical")
            der1 = der1*Bohr # because A^{-1}
            # print("desc1",desc1.shape)
            K_final = np.zeros((len(desc1),len(self.representing_set_descriptors[iddesc])))
            ldesc = len(desc1[0])
            lder = der1[0].shape
            el_descs = {}
            el_ders = {}
            dKdr = {} 
            dK = np.zeros((len(desc1),3,len(desc1),len(self.representing_set_descriptors[iddesc])))
            for el in self.elements:
                el_descs[el] = []
                el_ders[el] = []
                dKdr[el] = []
                if self.perEl == True:
                    for countRepre in range(len(desc1)):
                        if el == elems1[countRepre]:
                            el_descs[el].append(desc1[countRepre])
                        else:
                            el_descs[el].append(np.zeros(ldesc))
                elif self.perEl == False:
                    el_descs[el] = desc1
                el_descs[el] = np.array(el_descs[el])
                K_temp = np.matmul(np.array(el_descs[el]),np.transpose(np.array(self.repres_descs[iddesc][el])))**self.zeta
                K_final += K_temp
                t1 = (np.matmul(el_descs[el],np.transpose(self.repres_descs[iddesc][el]))**(self.zeta-1))
                for idAt,nl in enumerate(nl1):
                    t2 = np.matmul(der1[idAt][0],np.transpose(self.repres_descs[iddesc][el]))   
                    dK[nl[1],0,nl[0]] += self.zeta*np.multiply(t1[nl[0]],t2)
                    t2 = np.matmul(der1[idAt][1],np.transpose(self.repres_descs[iddesc][el]))   
                    dK[nl[1],1,nl[0]] += self.zeta*np.multiply(t1[nl[0]],t2)
                    t2 = np.matmul(der1[idAt][2],np.transpose(self.repres_descs[iddesc][el]))
                    dK[nl[1],2,nl[0]] += self.zeta*np.multiply(t1[nl[0]],t2)
            alldK.append(dK)
            allK.append(K_final)
            # print(f"Computation of descriptors ({K_final.shape}) and derivatives ({dK.shape}) of descriptor {iddesc} is complated")
        return allK,alldK
    
    def _create_representationPrepicked(self,nameS="KQEqSOAP.npy",nameEl="KQEqEl.npy"):
        for iddesc in range(len(self.Kernel.Descriptors)):
            np.save(f"{nameS}_{iddesc}" , self.Kernel.representing_set_descriptors[iddesc])
            np.save(f"{nameEl}_{iddesc}", self.Kernel.representing_set_elements[iddesc])
    # def kernel_derivatives(self,mol_set1,zeta=2):
        # descriptors_train,derivatives_train,derivatives_nl_train, elements_train = self.Descriptors[0].calc_batch_descriptor_derivative(self.training_set)
        # descriptors_represent.extend([soapVecs[element][pos] for pos in picked]) = descriptors_train[self.sparse_points]
        # derivatives_represent = derivatives_train[self.sparse_points]
        # derivatives_nl_represent = derivatives_nl_train[self.sparse_points]
        # elements_represent = elements_train[self.sparse_points]
        # print(descriptors_represent.shape,derivatives_represent.shape,derivatives_nl_represent.shape,elements_represent.shape )
        '''
        alldK = []
        allK = []
        for iddesc in range(len(self.Descriptors)):
            desc1,der1,nl1,elems1 = self.Descriptors[iddesc].calc_descriptor_derivative(mol_set1)#,deriv="numerical")
            K_final = np.zeros((len(desc1),len(self.representing_set_descriptors[iddesc])))
            ldesc = len(desc1[0])
            lder = der1[0].shape
            el_descs = {}
            el_ders = {}
            dKdr = {} 
            dK = np.zeros((len(desc1),3,len(desc1),len(self.representing_set_descriptors[iddesc])))
            for el in self.elements:
                el_descs[el] = []
                el_ders[el] = []
                dKdr[el] = []
                if self.perEl == True:
                    for countRepre in range(len(desc1)):
                        if el == elems1[countRepre]:
                            el_descs[el].append(desc1[countRepre])
                        else:
                            el_descs[el].append(np.zeros(ldesc))
                elif self.perEl == False:
                    el_descs[el] = desc1
                el_descs[el] = np.array(el_descs[el])
                K_temp = np.matmul(np.array(el_descs[el]),np.transpose(np.array(self.repres_descs[iddesc][el])))**zeta
                K_final += K_temp                
                t1 = (np.matmul(el_descs[el],np.transpose(self.repres_descs[iddesc][el]))**(zeta-1))
                for idAt,nl in enumerate(nl1):
                    t2 = np.matmul(der1[idAt][0],np.transpose(self.repres_descs[iddesc][el]))   
                    dK[nl[1],0,nl[0]] += zeta*np.multiply(t1[nl[0]],t2)
                    t2 = np.matmul(der1[idAt][1],np.transpose(self.repres_descs[iddesc][el]))   
                    dK[nl[1],1,nl[0]] += zeta*np.multiply(t1[nl[0]],t2)
                    t2 = np.matmul(der1[idAt][2],np.transpose(self.repres_descs[iddesc][el]))
                    dK[nl[1],2,nl[0]] += zeta*np.multiply(t1[nl[0]],t2)
            alldK.append(dK)
            allK.append(K_final)
            # print(f"Computation of descriptors ({K_final.shape}) and derivatives ({dK.shape}) of descriptor {iddesc} is complated")
        return allK,alldK
        '''
            
    def kernel_matrices(self,mol_set1=None,mol_set2=None,kerneltype='predicting'):
        if kerneltype=="training":
            print("Computed kernel for training")
            K_nms = []
            K_mms = []
            desc1 = self.train_descs
            desc2 = self.repres_descs
            for iddesc in range(len(self.Descriptors)):
                K_nm = self.kernel_matrix(desc1[iddesc],desc2[iddesc],desc_kernel=self.Descriptors[iddesc].desc_kernel)
                K_mm = self.kernel_matrix(desc2[iddesc],desc2[iddesc],desc_kernel=self.Descriptors[iddesc].desc_kernel)
                # K_mm = np.matmul(self.representing_set_descriptors[0],self.representing_set_descriptors[0].T)#self.kernel_matrix(desc2[iddesc],desc2[iddesc],desc_kernel=self.Descriptors[iddesc].desc_kernel)
                K_nms.append(K_nm)
                K_mms.append(K_mm)
            print("Kernel is done")
            return K_nms, K_mms
        elif kerneltype=="predicting":
            # print("Computed kernel for predicting")
            desc1 = []
            Ks = []
            desc2 = self.repres_descs
            for iddesc in range(len(self.Descriptors)):
                desc1,elems1 = self.Descriptors[iddesc].calc_descriptors(mol_set1)
                ldesc = len(desc1[0])
                el_descs = {}
                for el in self.elements:
                    el_descs[el] = []
                    for countRepre in range(len(desc1)):
                        if el == elems1[countRepre]:
                            el_descs[el].append(desc1[countRepre])
                        else:
                            el_descs[el].append(np.zeros(ldesc))
                    el_descs[el] = np.array(el_descs[el])
                K = self.kernel_matrix(el_descs,desc2[iddesc],desc_kernel=self.Descriptors[iddesc].desc_kernel)
                Ks.append(K)
            # print("Kernel is done")
            return Ks
            
    def kernel_matrix(self,desc1=None,desc2=None,N=0,M=0,desc_kernel="poly"):
        K = np.zeros((len(desc1[self.elements[0]]),len(desc2[self.elements[0]])))
        if desc_kernel == "poly":
            zeta = self.zeta
            for el in self.elements:
                K_temp = np.matmul(np.array(desc1[el]),np.transpose(np.array(desc2[el])))**zeta
                K += K_temp
        return K
