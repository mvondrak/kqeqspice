#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <iostream>
#include <vector>
// g++ -O3 -Wall -shared -std=c++11 -fPIC $(python -m pybind11 --includes) ewald.cpp -o ewaldCPP$(python-config --extension-suffix)
// g++ -O3 -fopenmp -Wall -shared -std=c++11 -fPIC $(python -m pybind11 --includes) ewald.cpp -o ewaldCPP_openmp$(python-config --extension-suffix)
namespace py = pybind11;

py::array derReal(long unsigned int nat,
            py::array_t<double> pos, 
            py::array_t<double> latPy, 
            py::array_t<double> sigmaPy, 
            py::array_t<double> qPy, 
            py::array_t<double> nPy,
            double eta, 
            double cutoff,
            py::array_t<double> pyAreal){

    std::vector<size_t> shape = {nat, nat, 3};
    py::array_t<double> result(shape);
    auto Areal = result.mutable_unchecked<3>();
    for (size_t i = 0; i < nat; ++i) {
        for (size_t j = 0; j < nat; ++j) {
            for (size_t k = 0; k < 3; ++k) {
                Areal(i, j, k) = 0.0; // Initialize all elements to 0.0
            }
        }
    }
    py::buffer_info bufhard1 = qPy.request();
    double *q = (double*) bufhard1.ptr;

    py::buffer_info bufsigma1 = sigmaPy.request();
    double *sigma = (double*) bufsigma1.ptr;

    py::buffer_info buflat1 = latPy.request();
    double *lat = (double*) buflat1.ptr;

    py::buffer_info bufats1 = pos.request();
    double *ats = (double*) bufats1.ptr;

    py::buffer_info bufn1 = nPy.request();
    double *n = (double*) bufn1.ptr;
    int count = 0;
    int lim1 = n[0]+1;
    int lim2 = n[1]+1;
    int lim3 = n[2]+1;
    // double dlat[3],d[3],d2,r, interf,invsqrt2eta, gamma,f0,f1,f2;
    double invsqrt2eta = 1 / (sqrt(2) * eta);
    // for (int i = 0; i < 9 ; i++)
    //     for (int j = 0; j < 9 ; j++)
    //         for (int d = 0; d < 3 ; d++)
    //             Areal[i][j][d] = 0.0;
    const double PI =3.14159265359;
#pragma omp parallel for shared(Areal, ats, lat, sigma, q, n) //private(dlat, d, d2, r, interf, gamma, f0, f1, f2)
    for (py::size_t iat=0; iat <nat; iat++){
        for (int i=-n[0]; i < lim1; i++){
            for (int j=-n[1]; j < lim2; j++){
                for (int k=-n[2]; k < lim3; k++){
		    double dlat[3];
		    dlat[0] = i*lat[0] + j*lat[3] +k*lat[6];
                    dlat[1] = i*lat[1] + j*lat[4] +k*lat[7];
                    dlat[2] = i*lat[2] + j*lat[5] +k*lat[8];
                    for (py::size_t jat=iat;jat<nat;jat++){
                        if (i != 0 || j != 0 || k!=0 || iat!=jat){
			    double d[3];
			    d[0] = ats[iat*3] - ats[jat*3] + dlat[0];
                            d[1] = ats[iat*3+1] - ats[jat*3+1] + dlat[1];
                            d[2] = ats[iat*3+2] - ats[jat*3+2] + dlat[2];
                            double d2 = d[0]*d[0] + d[1]*d[1] + d[2]*d[2];

                            if (d2 > cutoff*cutoff){
                                continue;
                            }

                            double r = sqrt(d2);

                            double interf = erfc(r*invsqrt2eta);

                            double gamma = sqrt(sigma[iat]*sigma[iat]+sigma[jat]*sigma[jat]);
                            interf = interf - erfc(r/(sqrt(2)*gamma));
                            double f0 = d[0]/(r*r*r)*(2*r*invsqrt2eta* exp(-(invsqrt2eta*invsqrt2eta)*r*r) /sqrt(PI)- 2* r/(sqrt(2)*gamma)*exp(-1/(2*gamma*gamma)*r*r)/sqrt(PI)+interf)/2;
                            double f1 = d[1]/(r*r*r)*(2*r*invsqrt2eta* exp(-(invsqrt2eta*invsqrt2eta)*r*r) /sqrt(PI)- 2* r/(sqrt(2)*gamma)*exp(-1/(2*gamma*gamma)*r*r)/sqrt(PI)+interf)/2;
                            double f2 = d[2]/(r*r*r)*(2*r*invsqrt2eta* exp(-(invsqrt2eta*invsqrt2eta)*r*r) /sqrt(PI)- 2* r/(sqrt(2)*gamma)*exp(-1/(2*gamma*gamma)*r*r)/sqrt(PI)+interf)/2;

                            count += 1;
                            Areal(iat,iat,0) = Areal(iat,iat,0) - f0*q[jat];
                            Areal(jat,jat,0) = Areal(jat,jat,0) + f0*q[iat];
                            Areal(jat,iat,0) = Areal(jat,iat,0) - f0*q[iat];
                            Areal(iat,jat,0) = Areal(iat,jat,0) + f0*q[jat];
                            Areal(iat,iat,1) = Areal(iat,iat,1) - f1*q[jat];
                            Areal(jat,jat,1) = Areal(jat,jat,1) + f1*q[iat];
                            Areal(jat,iat,1) = Areal(jat,iat,1) - f1*q[iat];
                            Areal(iat,jat,1) = Areal(iat,jat,1) + f1*q[jat];
                            Areal(iat,iat,2) = Areal(iat,iat,2) - f2*q[jat];
                            Areal(jat,jat,2) = Areal(jat,jat,2) + f2*q[iat];
                            Areal(jat,iat,2) = Areal(jat,iat,2) - f2*q[iat];
                            Areal(iat,jat,2) = Areal(iat,jat,2) + f2*q[jat];
			}
		    }
		}
	    }
	}
    }
    return result;
}



py::array derRecip(long unsigned int nat,
                py::array_t<double> pos, 
                py::array_t<double> reclatPy, 
                py::array_t<double> nPy, 
                double eta, 
                double cutoff, 
                py::array_t<double> qPy, 
                py::array_t<double> pyArecip){
    py::buffer_info buflat1 = reclatPy.request();
    double *reclat = (double*) buflat1.ptr;
    py::buffer_info bufats1 = pos.request();
    double *ats = (double*) bufats1.ptr;
    py::buffer_info bufn1 = nPy.request();
    double *n = (double*) bufn1.ptr;
    py::buffer_info bufq1 = qPy.request();
    double *q = (double*) bufq1.ptr;
    // auto Arecip = pyArecip.mutable_unchecked<3>();
    // ssize_t t = 3;
    std::vector<size_t> shape = {nat, nat, 3};
    py::array_t<double> result(shape);
    auto Arecip = result.mutable_unchecked<3>();
    for (size_t i = 0; i < nat; ++i) {
        for (size_t j = 0; j < nat; ++j) {
            for (size_t k = 0; k < 3; ++k) {
                Arecip(i, j, k) = 0.0; // Initialize all elements to 0.0
            }
        }
    }
    double dlat[3], r, factor, kri, krj, dAdxi;//X, dAdxiY, dAdxiZ;
    // for (int i = 0; i < 9 ; i++)
    //     for (int j = 0; j < 9 ; j++)
    //         for (int d = 0; d < 3 ; d++)
    //         {
    //             Arecip[i][j][d] = 0.0;
    //         }
    int lim1 = n[0] + 1;
    int lim2 = n[1] + 1;
    int lim3 = n[2] + 1;
    // #pragma omp parallel for
    #pragma omp parallel for private(dlat, r, factor, kri, krj, dAdxi) shared(lim1,lim2,lim3,reclat,Arecip) collapse(3)
    for (int i=-n[0]; i < lim1; i++){
        for (int j=-n[1]; j < lim2; j++){
            for (int k=-n[2]; k <lim3; k++){
                if (i != 0 || j != 0 || k != 0){
                    dlat[0] = i*reclat[0] + j*reclat[3] +k*reclat[6];
                    dlat[1] = i*reclat[1] + j*reclat[4] +k*reclat[7];
                    dlat[2] = i*reclat[2] + j*reclat[5] +k*reclat[8];
                    r = dlat[0]*dlat[0] + dlat[1]*dlat[1] + dlat[2]*dlat[2];
                    if (r > cutoff*cutoff){
                        continue;
                    }
                    
                    factor = exp(-eta*eta*r/2)/r;
                    for (py::size_t iat=0; iat < nat; iat++){
                        kri = dlat[0]*ats[iat*3] + dlat[1]*ats[iat*3+1] + dlat[2]*ats[iat*3+2];
                        for (py::size_t jat = iat; jat < nat; jat++){
                            krj = dlat[0]*ats[jat*3] + dlat[1]*ats[jat*3+1] + dlat[2]*ats[jat*3+2];
			    for (py::size_t direction = 0 ; direction < 3 ; direction++){
                            dAdxi = factor * (-sin(kri) * dlat[direction] * cos(krj) + cos(kri) * dlat[direction] * sin(krj));
                            //dAdxiY = factor * (-sin(kri) * dlat[1] * cos(krj) + cos(kri) * dlat[1] * sin(krj));
			    //dAdxiZ = factor * (-sin(kri) * dlat[2] * cos(krj) + cos(kri) * dlat[2] * sin(krj));
			    #pragma omp atomic
			    Arecip(iat,iat,direction) += dAdxi * q[jat];
			    #pragma omp atomic
			    Arecip(jat,iat,direction) += dAdxi * q[iat];
			    #pragma omp atomic
			    Arecip(jat,jat,direction) -= dAdxi * q[iat];
			    #pragma omp atomic
			    Arecip(iat,jat,direction) -= dAdxi * q[jat];
			    }
			}
		    }
		}
	    }
	}
    }
    return result;
}

void ewaldReal(int nat,py::array_t<double> pos, py::array_t<double> latPy, py::array_t<double> sigmaPy, py::array_t<double> hardPy, py::array_t<double> nPy,double eta, double cutoff,py::array_t<double> pyAreal)
{
    // py::gil_scoped_acquire acquire;
    // py::gil_scoped_acquire acquire;
    py::buffer_info bufhard1 = hardPy.request();
    double *hard = (double*) bufhard1.ptr;

    py::buffer_info bufsigma1 = sigmaPy.request();
    double *sigma = (double*) bufsigma1.ptr;

    py::buffer_info buflat1 = latPy.request();
    double *lat = (double*) buflat1.ptr;

    py::buffer_info bufats1 = pos.request();
    double *ats = (double*) bufats1.ptr;

    py::buffer_info bufn1 = nPy.request();
    double *n = (double*) bufn1.ptr;

    auto Areal = pyAreal.mutable_unchecked<2>();

    double dlat[3],d[3],d2,r, interf,invsqrt2eta, gamma;
    invsqrt2eta = 1 / (sqrt(2) * eta);
    const double PI =3.14159265359;
    int lim1 = n[0]+1;
    int lim2 = n[1]+1;
    int lim3 = n[2]+1;
    // #pragma omp parallel for simd ordered  private(dlat, d, d2, r, interf, gamma) shared(Areal, ats, lat, sigma, hard, n) schedule(dynamic)
    #pragma omp parallel for private(dlat, d, d2, r, interf, gamma) shared(Areal, ats, lat, sigma, hard, n)// schedule(static) reduction(+:Areal) 
    for (int iat=0; iat <nat; iat++){
        for (int i=-n[0]; i < lim1; i++){
            for (int j=-n[1]; j < lim2; j++){
                for (int k=-n[2]; k < lim3; k++){


                    dlat[0] = i*lat[0] + j*lat[3] +k*lat[6];
                    dlat[1] = i*lat[1] + j*lat[4] +k*lat[7];
                    dlat[2] = i*lat[2] + j*lat[5] +k*lat[8];

                    // std::cout << dlat[0] << " " << dlat[1] << " " << dlat[2] << std::endl;
                    for (int jat=iat;jat<nat;jat++){
                        if (i != 0 || j != 0 || k!=0 || iat!=jat){
                            d[0] = ats[iat*3] - ats[jat*3] + dlat[0];
                            d[1] = ats[iat*3+1] - ats[jat*3+1] + dlat[1];
                            d[2] = ats[iat*3+2] - ats[jat*3+2] + dlat[2];
                            d2 = d[0]*d[0] + d[1]*d[1] + d[2]*d[2];

                            if (d2 > cutoff*cutoff){
                                continue;
                            }

                            r = sqrt(d2);
                                                     
                            interf = erfc(r*invsqrt2eta);
                            
                            
                            gamma = sqrt(sigma[iat]*sigma[iat]+sigma[jat]*sigma[jat]);
                            interf = interf - erfc(r/(sqrt(2)*gamma));
                            // #pragma omp atomic
                            Areal(iat,jat) += interf/r;}                            
		    }
		}
	    }
	}
    //    #pragma omp atomic    
        Areal(iat,iat) = Areal(iat,iat) + 1/(sigma[iat]*sqrt(PI)) + hard[iat];
    }
    
    for (int iat = 0; iat < nat; iat ++){
        for (int jat = 0; jat < nat; jat ++){
            Areal(jat,iat) = Areal(iat,jat);}}
    
    // Areal.resize({nat,nat});
    // py::gil_scoped_release release;

    // return Areal;
}


void ewaldRecip(int nat,py::array_t<double> pos, py::array_t<double> reclatPy, py::array_t<double> nPy,double eta, double cutoff,double V,py::array_t<double> pyArecip)
{
    // py::gil_scoped_acquire acquire;
    // py::gil_scoped_acquire acquire;
    py::buffer_info buflat1 = reclatPy.request();
    double *reclat = (double*) buflat1.ptr;

    py::buffer_info bufats1 = pos.request();
    double *ats = (double*) bufats1.ptr;

    py::buffer_info bufn1 = nPy.request();
    double *n = (double*) bufn1.ptr;


    // py::array_t<double> Arecip = py::array_t<double>(nat*nat);
    // py::buffer_info buf1 = Arecip.request();
    // double *ptr1 = (double *) buf1.ptr;
    auto Arecip = pyArecip.mutable_unchecked<2>();    

    double dlat[3],r, factor, kri,krj, temp;

    int lim1 = n[0]+1;
    int lim2 = n[1]+1;
    int lim3 = n[2]+1;
    // #pragma omp parallel for private(dlat, r, factor, kri, krj) shared(Arecip, ats, reclat, n) schedule(static)
    // #pragma omp parallel for private(dlat, r, factor, kri, krj) default(shared) schedule(static)
    #pragma omp parallel for private(dlat, r, factor, kri, krj, temp) shared(Arecip, ats, reclat, n)
    for (int i=-n[0]; i < lim1; i++){
        for (int j=-n[1]; j < lim2; j++){
            for (int k=-n[2]; k < lim3; k++){
                dlat[0] = i*reclat[0] + j*reclat[3] +k*reclat[6];
                dlat[1] = i*reclat[1] + j*reclat[4] +k*reclat[7];
                dlat[2] = i*reclat[2] + j*reclat[5] +k*reclat[8];
                if (i != 0 || j != 0 || k!=0){
                    r = dlat[0]*dlat[0] + dlat[1]*dlat[1] + dlat[2]*dlat[2];
                    if (r > cutoff*cutoff){
                        continue;
                    }
                    factor = exp(-eta*eta*r/2)/r;
                    for (int iat=0; iat <nat; iat++){
                        kri =dlat[0]*ats[iat*3] + dlat[1]*ats[iat*3+1] + dlat[2]*ats[iat*3+2];
                        for (int jat=iat;jat<nat;jat++){
                            krj =dlat[0]*ats[jat*3] + dlat[1]*ats[jat*3+1] + dlat[2]*ats[jat*3+2];
                            temp = factor*(cos(kri)*cos(krj)+sin(kri)*sin(krj));
                            #pragma omp atomic
                            Arecip(iat,jat) +=  temp;
			}
		    }
		}
	    }
	}
    }
    
    for (int iat = 0; iat < nat; iat ++){
        for (int jat = 0; jat < nat; jat ++){
            Arecip(jat,iat) = Arecip(iat,jat);
	}
    }
}





void ewaldSelf(int nat,double eta,py::array_t<double> pyAself){
        auto Aself = pyAself.mutable_unchecked<2>();    
    for (int i = 0; i < nat ; i++){
        Aself(i,i) = -2/(sqrt(2*3.14159265359)*eta);
    }
}
PYBIND11_MODULE(ewaldCPP, m) {
    m.def("ewaldReal",&ewaldReal);//,py::call_guard<py::gil_scoped_release>());
    m.def("ewaldRecip",&ewaldRecip);//,py::call_guard<py::gil_scoped_release>());
    m.def("ewaldSelf",&ewaldSelf);//,py::call_guard<py::gil_scoped_release>());
    m.def("derRecip",&derRecip);
    m.def("derReal",&derReal);
}
