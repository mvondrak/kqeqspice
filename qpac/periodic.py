from qpac.ewaldCPP import ewaldReal, ewaldRecip, ewaldSelf,derRecip,derReal
from qpac.ewaldA import dAxyzQReal, dAxyzQRecip
# import kqeq.ewaldCPP
import numpy as np

ewaldSummationPrecision = 1E-8


def getRecip(nAt,xyz,lattice,atsize,hard):
    reclat = recLattice(lattice)
    eta = getOptimalEtaMatrix(lattice)
    eta = np.max([eta,np.max(atsize)])
    cutoffReal = getOptimalCutoffReal(eta,ewaldSummationPrecision)
    cutoffRecip = getOptimalCutoffRecip(eta,ewaldSummationPrecision)
    nReal = getNcells(lattice,cutoffReal)
    nRecip = getNcells(reclat,cutoffRecip)
    # print(nRecip)
    V = np.linalg.det(lattice)
    Real = np.zeros((nAt,nAt))
    Recip = np.zeros((nAt,nAt))
    Aself = np.zeros((nAt,nAt))
    ewaldRecip(nAt,xyz,reclat,nRecip,eta,cutoffRecip,V,Recip)
    Recip = Recip/V *4*np.pi
    return Recip

def getReal(nAt,xyz,lattice,atsize,hard):
    reclat = recLattice(lattice)
    eta = getOptimalEtaMatrix(lattice)
    eta = np.max([eta,np.max(atsize)])
    cutoffReal = getOptimalCutoffReal(eta,ewaldSummationPrecision)
    cutoffRecip = getOptimalCutoffRecip(eta,ewaldSummationPrecision)
    nReal = getNcells(lattice,cutoffReal)
    nRecip = getNcells(reclat,cutoffRecip)
    # print(nRecip)
    V = np.linalg.det(lattice)
    Real = np.zeros((nAt,nAt))
    Recip = np.zeros((nAt,nAt))
    Aself = np.zeros((nAt,nAt))
    ewaldReal(nAt,xyz,lattice,atsize,hard,nReal,eta,cutoffReal,Real)
    return Real

def eemMatrixEwald(nAt,xyz,lattice,atsize,hard):
    reclat = recLattice(lattice)
    eta = getOptimalEtaMatrix(lattice)
    eta = np.max([eta,np.max(atsize)])
    cutoffReal = getOptimalCutoffReal(eta,ewaldSummationPrecision)
    cutoffRecip = getOptimalCutoffRecip(eta,ewaldSummationPrecision)
    nReal = getNcells(lattice,cutoffReal)
    nRecip = getNcells(reclat,cutoffRecip)
    # print(nRecip)
    V = np.linalg.det(lattice)
    Real = np.zeros((nAt,nAt))
    Recip = np.zeros((nAt,nAt))
    Aself = np.zeros((nAt,nAt))
    # ewaldReal(nAt,xyz,lattice,atsize,hard,nReal,eta,cutoffReal,Real)
    ewaldRecip(nAt,xyz,reclat,nRecip,eta,cutoffRecip,V,Recip)
    Recip = Recip/V *4*np.pi
    ewaldSelf(nAt,eta,Aself)
    res =  Recip # Recip + Real + Aself
    return res 

def derA(nat,ats,lat,sigma,q):
    eta = getOptimalEtaMatrix(lat)
    # eta = getOptimalEta(lat,nat)
    eta = np.max([eta,np.max(sigma)])
    cutoff = getOptimalCutoffReal(eta,ewaldSummationPrecision)
    n = getNcells(lat,cutoff)
    reclat = recLattice(lat)
    V = np.linalg.det(lat)
    cutoffReal = getOptimalCutoffReal(eta,ewaldSummationPrecision)
    cutoffRecip = getOptimalCutoffRecip(eta,ewaldSummationPrecision)
    nReal = getNcells(lat,cutoffReal)
    nRecip = getNcells(reclat,cutoffRecip)
    # tempReal = dAxyzQReal(nat,ats,lat,sigma,q,eta,n,cutoff)
    # tempRec = dAxyzQRecip(nat,ats,reclat,n,eta,cutoff,V,q)
    tempRec = np.zeros((nat,nat,3))
    tempReal = np.zeros((nat,nat,3))
    tempRec = derRecip(nat,ats,reclat,nRecip,eta,cutoffRecip,q,tempRec)
    # tempReal = derReal(nat,ats,lat,sigma,q,nReal,eta,cutoffReal,tempReal)
    tempRec = tempRec/V *4*np.pi/2
    return tempRec# + tempRec


def getOptimalEtaMatrix(lat):
    return 1/np.sqrt(2*np.pi)*np.linalg.det(lat)**(1/3)

def getOptimalEta(lat,nat):
    return 1 / np.sqrt(2 * np.pi) * (np.linalg.det(lat)**2 / nat)**(1 / 6)

def getOptimalCutoffReal(eta,A):
    r = np.sqrt(2)*eta*np.sqrt(-np.log(A))
    return r

def getOptimalCutoffRecip(eta,A):
    r = (np.sqrt(2)/eta)*np.sqrt(-np.log(A))
    return r

def recLattice(At):
    # At = At.T
    B = (np.linalg.inv(At.T)*(2*np.pi))
    return B

def getNcells(lat,cutoff):
    proj = np.zeros(3)
    # axb = np.cross(lat[:,0],lat[:,1])
    axb = np.cross(lat[0],lat[1])
    axb = axb / np.sqrt(np.sum(axb**2))
    # axc = np.cross(lat[:,0], lat[:,2])
    axc = np.cross(lat[0], lat[2])
    axc = axc / np.sqrt(np.sum(axc**2))
    # bxc = np.cross(lat[:,1], lat[:,2])
    bxc = np.cross(lat[1], lat[2])
    bxc = bxc / np.sqrt(np.sum(bxc**2))
    proj[0] = np.dot(lat[0], bxc)
    proj[1] = np.dot(lat[1], axc)
    proj[2] = np.dot(lat[2], axb)
    n = np.ceil(cutoff/abs(proj))
    n = [int(a) for a in n]
    return n 
