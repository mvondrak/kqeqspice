import numpy as np
from ase.io import *
from ase.data import covalent_radii
from ase.units import Bohr
import random
from qpac.data import atomic_numbers
from quippy.descriptors import Descriptor
from dscribe.descriptors import SOAP
# from rascal.representations import SphericalInvariants

# from quippy.descriptors import Descriptor
np.random.seed(10)


class soapClass():
    def __init__(self):
        self.precomputed = {"training":[]}
        self.precomputedEl = {"training":[]}

    def compute(self,mols,name):
        soaps = self.calc_descriptors(mols)
        self.precomputed[name] = soaps[0]
        self.precomputedEl[name] = soaps[1]

    def saveSOAP(self,name,nameSave):
        np.save(nameSave,self.precomputed[name])
        np.save(f'El{nameSave}',self.precomputedEl[name])
    
    def loadSOAP(self,name,nameSave):
        self.precomputed[name] = np.load(nameSave)
        self.precomputedEl[name] = np.load(f'El{nameSave}')

    def calc_descriptors(mols):
        raise NotImplementedError


class quipSOAP(soapClass):
    def __init__(self,STRsoap,species,desc_kernel="poly"):
        self.species = species
        self.soap_hyper = STRsoap
        self.desc_kernel = desc_kernel
        self.dimEl = len(self.species)
        super().__init__()
        
    def calc_descriptors(self,molecules):
        element_list = []
        soap_descriptors=[]
        strSOAP = self.soap_hyper
        soap = Descriptor(strSOAP)
        soaps = soap.calc(molecules)
        for ids,mol in enumerate(soaps):
            element_list.extend([a.symbol for a in molecules[ids]])
            # for at in mol["data"]:
            soap_descriptors.extend(mol["data"])
        return np.array(soap_descriptors), np.array(element_list)



    def calc_descriptor_derivative(self,ref_mol):
        soap = Descriptor(self.soap_hyper)
        soaps = soap.calc(ref_mol, grad=True)
        element_list = []
        element_list.extend([a.symbol for a in ref_mol])
        element_list = np.array(element_list)

        desc1 = soaps["data"]
        der1 = soaps["grad_data"]
        nl = soaps["grad_index_0based"]
        return desc1, der1, nl,element_list

    def calc_batch_descriptor_derivative(self,molecules):
        soap_descriptors = []
        soap_derivatives = []
        soap_derivatives_nl = []
        soap = Descriptor(self.soap_hyper)
        soaps = soap.calc(molecules, grad=True)
        element_list = []
        for ids,mol in enumerate(soaps):
            element_list.extend([a.symbol for a in molecules[ids]])
            soap_descriptors.extend(mol["data"])
            soap_derivatives.extend(mol["grad_data"])
            soap_derivatives_nl.extend(mol["grad_index_0based"])
        element_list = np.array(element_list)
        return soap_descriptors, soap_derivatives, soap_derivatives_nl,element_list

    
        
# class turbo_soap(soapClass):
#     def __init__(self, STRsoap,species):
#         self.species = species
#         self.soap_hyper = STRsoap
#         self.dimEl = len(self.species)
#         # self.soap_hyper = "soap_turbo l_max=4 alpha_max={5 5} atom_sigma_r={0.5 0.5} atom_sigma_t={0.5 0.5} atom_sigma_r_scaling={0. 0.} atom_sigma_t_scaling={0. 0.} radial_enhancement=1 amplitude_scaling={1. 1.} central_weight={1.0 1.0}  basis=poly3gauss compress_mode=trivial rcut_hard=5.0 rcut_soft=4.5 n_species=2 species_Z={8 30} central_index=1 add_species=F"
#         super().__init__()

#     def descriptor(self,molecules):
#         element_list = []
#         soap_atoms=[]
#         for idel,el in enumerate(self.species):
#             strSOAP = self.soap_hyper + f' central_index={idel+1}'
#             soap = Descriptor(strSOAP)

#             soaps = soap.calc(molecules)
#             for ids,mol in enumerate(soaps):
#                 element_list.extend([a.symbol for a in molecules[ids]])
#                 soap_atoms.extend(mol["data"])
#         return np.array(soap_atoms), np.array(element_list)


class dscribeSOAP(soapClass):
    def __init__(self, nmax, lmax,rcut, sigma,species,delta,periodic,desc_kernel="poly"):
        self.species = species
        self.delta = delta
        self.desc_kernel=desc_kernel
        self.soap = SOAP(
            periodic=periodic,
            r_cut=rcut,  
            n_max=nmax,
            sigma=sigma,
            species = species,
            l_max=lmax)
        super().__init__()

    def descderSingle(self,molecule,centers,indices, deriv = "numerical"):
        desc_temp = self.soap.create_single(molecule,[centers])
        norm = np.linalg.norm(desc_temp)
        normDesc = desc_temp/norm
        der_temp = []
        for indice in indices:
            dir1 = self.num_soap(molecule,center = [centers],direction = 0, iatom = indice)
            dir2 = self.num_soap(molecule,center = [centers],direction = 1, iatom = indice)
            dir3 = self.num_soap(molecule,center = [centers],direction = 2, iatom = indice)
            directional = [dir1,dir2,dir3]
            der_temp.append(directional)
        der_temp = np.array(der_temp)
        der_temp = np.einsum('abcd->cabd',der_temp)
        vd = (np.einsum('ij,karj->ari',desc_temp,der_temp))
        norm = np.linalg.norm(desc_temp)
        vd = vd/norm
        r1 = np.einsum('akl,lj->aklj',vd,desc_temp,optimize="greedy")
        f1 = der_temp*norm
        f1 = np.einsum('abcd->bcad',f1)
        normDer = (f1-r1)/(norm*norm)
        normDer = np.einsum('abcd->cbad',normDer)
        return normDesc,normDer
        
    def calc_descriptors(self,molecules,n_jobs=1):
        soap_atoms = []
        element_list = []
        soaps_new = self.soap.create(molecules,n_jobs = n_jobs)
        if len(molecules) == 1:
            soaps_new = [soaps_new]
        for ids,m in enumerate(soaps_new):
            element_list.extend([a.symbol for a in molecules[ids]])
            norms = np.linalg.norm(m,axis=-1)
            soap_atoms.extend((self.delta**(2))*m/norms[:,None])
        return np.array(soap_atoms), np.array(element_list)

    
    def calc_descriptor_derivative(self,molecule, deriv = "numerical"):
        der, desc = self.soap.derivatives(molecule,method=deriv,attach=True) #analytical derivatives in SOAPs are not usable with 1.2.0 version        '''
        element_list = []
        element_list.extend([a.symbol for a in molecule])
        element_list = np.array(element_list)
        norms = np.linalg.norm(desc,axis=-1)
        norm_desc = desc/norms[:,None]
        norms2 = norms**2
        deriv_final = np.moveaxis(der,[0,1],[2,0])
        # normal_soap_vector = np.array(normal_soap_vector)
        hold = (np.einsum('ij,arkj->arik',desc,deriv_final,optimize="greedy"))
        vd = np.einsum('akii->aki',hold,optimize="greedy")
        r1 = np.einsum('akl,lj,l->aklj',vd,desc,1/norms,optimize="greedy")
        f1 = np.einsum('aklj,l->aklj',deriv_final,norms,optimize="greedy")
        normDer = np.einsum('aklj,l->aklj',f1-r1,1/norms2,optimize="greedy")
        
        return norm_desc, normDer, element_list

