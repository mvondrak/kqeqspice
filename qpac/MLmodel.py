from qpac.kQEq import kernel_qeq
from qpac.GAP import GAP
from qpac.utils import *
from qpac.qeq import charge_eq

import numpy as np

class MLmodel():
    def __init__(self, GAPclass,kQEqclass):
        self.kQEqclass = kQEqclass
        self.GAPclass = GAPclass
        # self.gapMs = len(self.GAPclass.elements)*self.GAPclass.Kernel.sparse_count*len(self.GAPclass.Kernel.Descriptors)
        # self.kQEqMs = len(self.GAPclass.elements)*self.kQEqclass.Kernel.sparse_count*len(self.kQEqclass.Kernel.Descriptors)
        # self.gapMs =  45
        # self.kQEqMs = 45


    def train(self, 
              targets = "dipole", 
              target_sigmas = 1, 
              charge_keyword='initial_charges',
              max_iter = 50,
              atom_energy=None,
              n_bad_iters = 5,
              scc_target=False,
              scc_sigma=0.1,
              energy_keyword="energy",
              forces_keyword="forces",
              verbose=True):
        train_nAts_array = []
        noniterative_targets_templates = ["charges","dipole"]
        iterative_targets_templates = ["energy","forces"]
        noniterative_targets_current = []
        noniterative_targets_current_sigmas = []
        iterative_targets_current = []
        iterative_targets_current_sigmas = []


        Knm,bothKmm = self.buildKs("training")
        KnmGAP = np.hstack(self.GAPclass.K_nms)
        KnmkQEq = np.hstack(self.kQEqclass.K_nms)

        for id_t in range(len(targets)):
            if targets[id_t] in noniterative_targets_templates: 
                noniterative_targets_current.append(targets[id_t])
                noniterative_targets_current_sigmas.append(target_sigmas[id_t])
            if targets[id_t] in iterative_targets_templates: 
                iterative_targets_current.append(targets[id_t])
                iterative_targets_current_sigmas.append(target_sigmas[id_t])
        for mol in self.GAPclass.Kernel.training_set:
            train_nAts_array.append(len(mol))
        ones_for_Qmatrix = np.array(create_L_matrix(train_nAts_array))
        oldRMSE = np.inf
        E_ref = get_energies_atomic(mols=self.GAPclass.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
        scc_training = False
        count_iter = 0
        if charge_keyword != None:
            q_ref_iterative = get_charges(self.kQEqclass.Kernel.training_set,charge_keyword=charge_keyword)
        if len(iterative_targets_current) == 0:
            max_iter = 0
            
        # iterative_targets_current = ["energy"]
        training_on = True
        KnmGAP = np.hstack(self.GAPclass.K_nms)
        KnmkQEq = np.hstack(self.kQEqclass.K_nms)
        Xback = self.kQEqclass._get_Xback()
        A,Abar,Ainv = self.kQEqclass._build_As(self.kQEqclass.Kernel.training_set)
        O = self.kQEqclass.O_train
        X = self.kQEqclass.X_train
        bad = 1
        scc_training = False
        if "forces" in iterative_targets_current:
            K_GAP = []
            dK_GAP = []
            K_kQEq = []
            dK_kQEq  = []
            for mol in self.GAPclass.Kernel.training_set:
                Ks,dKdrs  = self.GAPclass.Kernel._calculate_function(mol_set1 = mol)
                K_GAP.append(Ks)
                dK_GAP.append(dKdrs)
            for mol in self.kQEqclass.Kernel.training_set:
                Ks,dKdrs  = self.kQEqclass.Kernel._calculate_function(mol_set1 = mol)
                K_kQEq.append(Ks)
                dK_kQEq.append(dKdrs)
        while training_on:
            if verbose == True:
                print("####################################################################")
            for id_target in range(0,len(iterative_targets_current)):
                if iterative_targets_current[id_target] == "energy":
                    #######kQEq part ###################################################
                    L, b_plus,ref = self.kQEqclass._process_data_iterative(iterative_targets_current[id_target],q_ref_iterative,atom_energy,energy_keyword,forces_keyword)
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    try:
                        b_up_kQEq = np.concatenate((b_up_kQEq,(-ref + b_plus)*diag_sigmas_sqrt))
                        B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([KnmkQEq.T,L])
                        B_up_kQEq = np.concatenate((B_up_kQEq,B_up_temp),axis=1)
                    except:
                        b_up_kQEq = diag_sigmas_sqrt*(-ref + b_plus) 
                        B_up_kQEq = diag_sigmas_sqrt*np.linalg.multi_dot([KnmkQEq.T,L])
                    #######GAP part ###################################################
                    for mol in self.GAPclass.Kernel.training_set:
                        ref = 1*get_energies_atomic(mols=self.GAPclass.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
                        K = np.array(self.GAPclass.Kernel.kernel_matrices(mol_set1 = [mol],kerneltype="predicting"))
                        LK = np.sum(K,axis=1)
                        try:
                            allKLsenergy = np.concatenate([allKLsenergy,np.sum(K,axis=1)],axis=0)
                        except:
                            allKLsenergy = np.sum(K,axis=1)
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    b_up_GAP = diag_sigmas_sqrt*ref 
                    B_up_GAP = diag_sigmas_sqrt*allKLsenergy.T
                elif iterative_targets_current[id_target] == "forces":
                    print("sigma",iterative_targets_current_sigmas[id_target])
                    #######kQEq part ###################################################
                    allKLsforces = None
                    current_position_for_force = 0
                    qeq_forces = []
                    for id_mol,mol in enumerate(self.kQEqclass.Kernel.training_set):
                        current_charges = q_ref_iterative[current_position_for_force:current_position_for_force+len(mol)]                         
                        current_position_for_force += len(mol)
                        # Ks,dKdrs  = self.kQEqclass.Kernel._calculate_function(mol_set1 = mol)
                        Ks = K_kQEq[id_mol]
                        dKdrs = dK_kQEq[id_mol]
                        qe = charge_eq(mol,Qtot=0,q=current_charges,scale_atsize=self.kQEqclass.scale_atsize,radius_type=self.kQEqclass.radius_type, hard=self.kQEqclass.hard_lib,periodic = self.kQEqclass.periodic)
                        # qe.calc_Charges()
                        qe.calc_Fqeq()
                        qeq_forces.extend((qe.f).flatten())#*(Hartree/Bohr))
                        dKdrs = np.array(dKdrs)
                        current_LK = np.einsum("kadAe,A->kade",dKdrs,current_charges) #k - number of kernels, a - soap derivatives of these atom, d - direction (3), A - wrt to this charges, e - number of weigts
                        current_LK = current_LK.reshape(-1,dKdrs.shape[-1])

                        try:
                            allKLsforces = np.concatenate((allKLsforces,current_LK))
                        except:
                            allKLsforces = current_LK

                    ref = -1*get_forces_atomic(mols=self.kQEqclass.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    b_up_temp = diag_sigmas_sqrt*(-ref - np.array(qeq_forces))
                    B_up_temp = diag_sigmas_sqrt*allKLsforces.T
                    # print("forces test",B_up_temp.shape,b_up_temp.shape,diag_sigmas_sqrt.shape,allKLsforces.shape)
                    try:
                        b_up_kQEq = np.concatenate((b_up_kQEq,b_up_temp))
                        # B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                        B_up_kQEq = np.concatenate((B_up_kQEq,B_up_temp),axis=1)
                    except:
                        B_up_kQEq = b_up_temp 
                        B_up_kQEq = B_up_temp #diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                    #######GAP part ###################################################
                    allKLsforces = None
                    for id_mol,mol in enumerate(self.GAPclass.Kernel.training_set):
                        # Ks,dKdrs  = self.GAPclass.Kernel._calculate_function(mol_set1 = mol)
                        Ks = K_GAP[id_mol]
                        dKdrs = dK_GAP[id_mol]
                        current_dKdrs = np.array(dKdrs)
                        current_LK = np.einsum("abcde->abce",dKdrs).reshape(-1,current_dKdrs.shape[-1])
                        try:
                            allKLsforces = np.concatenate((allKLsforces,current_LK))
                        except:
                            allKLsforces = current_LK
                    # ref = -1*get_forces_eV(mols=self.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                    ref = -1*get_forces_atomic(mols=self.GAPclass.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    # iterative_targets_current_sigmas[id_target] = 0.5*iterative_targets_current_sigmas[id_target]
                    b_up_temp = diag_sigmas_sqrt*(ref)
                    B_up_temp = diag_sigmas_sqrt*allKLsforces.T
                    try:
                        B_up_GAP = np.concatenate((B_up_GAP,B_up_temp),axis=1)
                        b_up_GAP = np.concatenate((b_up_GAP,b_up_temp))
                    except:
                        B_up_GAP = B_up_temp
                        b_up_GAP = b_up_temp                    

            #training_on = False
            # b_up_both = np.concatenate((b_up_GAP,b_up_kQEq))
            if scc_training:
                L, b_plus,ref = self.kQEqclass._process_data_iterative("scc",q_ref_iterative,atom_energy,energy_keyword,forces_keyword)
                diag_sigmas_sqrt = np.sqrt((1/scc_sigma))*np.ones(len(ref))
                b_up_kQEq = np.concatenate((b_up_kQEq,(ref + b_plus)*diag_sigmas_sqrt))
                B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([KnmkQEq.T,L])
                B_up_kQEq = np.concatenate((B_up_kQEq,B_up_temp),axis=1)
                scc_sigma = scc_sigma*0.5
                B_up_GAP = np.concatenate((B_up_GAP,np.zeros_like(B_up_temp)),axis=1)
            
            # print(B_up_GAP.shape,B_up_kQEq.shape)
            # print(B_up_GAP.shape,B_up_kQEq.shape)
            B_up_both = np.vstack((B_up_GAP,B_up_kQEq))
            Umm = np.linalg.cholesky(bothKmm).T
            B=np.concatenate((B_up_both.T,Umm))
            # print(b_up_both)
            b = np.concatenate((b_up_kQEq,np.zeros(bothKmm.shape[0])))
            # print("Umm",Umm.shapei)
            Q,R = np.linalg.qr(B) #,mode="complete")
            Qb = np.dot(Q.T,b)
            weights = -np.linalg.solve(R,Qb)
            # final_weight = weights
            weightsGAP = weights[:KnmGAP.shape[1]]
            weightskQEq = weights[KnmGAP.shape[1]:]
            eneg = np.matmul(KnmkQEq,weightskQEq)#/Hartree
            eneg_tot = (np.matmul(X,np.transpose(eneg)))
            eneg_tot = eneg_tot + O
            charge_temp = np.matmul(Ainv,-eneg_tot)
            q_new = np.matmul(Xback,charge_temp)
            Qmatrix_temp = ones_for_Qmatrix*q_new
            Cs_temp = 0.5*Qmatrix_temp@Abar@Qmatrix_temp.T
            Cs_temp = Cs_temp.sum(axis=0)
            Ekqeq =  Cs_temp + np.linalg.multi_dot([Qmatrix_temp,eneg])
            Egap = np.linalg.multi_dot([ones_for_Qmatrix,KnmGAP,weightsGAP])
            whole_energy = Egap + Ekqeq
            # print(Ekqeq)
            MSE = np.square(np.subtract(E_ref,whole_energy)).mean()
            RMSE_E = np.sqrt(MSE)
            B_up_both = None
            B_up_kQEq = None
            B_up_GAP = None
            b_up_both = None
            b_up_kQEq = None
            b_up_GAP = None
            B = None
            b = None
            allKLsenergy = None
            ###########################
            if verbose == True:
                print("New RMSE:",RMSE_E)
                print("Old RMSE:",oldRMSE)
                print("Bad RMSEs in row:", bad)
                print("Sample of charges:",q_new[-10:])
                print("sum of charges: ",np.sum(q_new))
                print(f"iteration {count_iter} is done")
                print("####################################################################")
            if (RMSE_E > oldRMSE and bad >= n_bad_iters) or (count_iter == max_iter):
                training_on = False
                # final_weight = weights
            elif RMSE_E > oldRMSE and bad < n_bad_iters and scc_training == False:
                bad += 1
                q_ref_iterative = q_new
                # final_weight = weights # delete this later
                if scc_target == True:
                    print("Activating scc")
                    scc_training = True
            elif RMSE_E > oldRMSE and bad < n_bad_iters and scc_training == True:
                bad += 1
                q_ref_iterative = q_new
            else:
                print("New best model reached")
                bad = 1
                final_weight = weights
                oldRMSE = RMSE_E
                q_ref_iterative = q_new
            count_iter += 1
        print("Lenght of weights:",len(final_weight))
        self.weights = final_weight
        self.GAPclass.weights = final_weight[:KnmGAP.shape[1]]
        self.kQEqclass.weights = final_weight[KnmGAP.shape[1]:]

    def train_followed(self, 
              targets = "dipole", 
              target_sigmas = 1, 
              charge_keyword='initial_charges',
              max_iter = 50,
              atom_energy=None,
              n_bad_iters = 5,
              scc_target=False,
              scc_sigma=0.1,
              energy_keyword="energy",
              forces_keyword="forces",
              verbose=True):
        train_nAts_array = []
        noniterative_targets_templates = ["charges","dipole"]
        iterative_targets_templates = ["energy","forces"]
        noniterative_targets_current = []
        noniterative_targets_current_sigmas = []
        iterative_targets_current = []
        iterative_targets_current_sigmas = []


        Knm,bothKmm = self.buildKs("training")
        KnmGAP = np.hstack(self.GAPclass.K_nms)
        KnmkQEq = np.hstack(self.kQEqclass.K_nms)

        for id_t in range(len(targets)):
            if targets[id_t] in noniterative_targets_templates: 
                noniterative_targets_current.append(targets[id_t])
                noniterative_targets_current_sigmas.append(target_sigmas[id_t])
            if targets[id_t] in iterative_targets_templates: 
                iterative_targets_current.append(targets[id_t])
                iterative_targets_current_sigmas.append(target_sigmas[id_t])
        for mol in self.GAPclass.Kernel.training_set:
            train_nAts_array.append(len(mol))
        ones_for_Qmatrix = np.array(create_L_matrix(train_nAts_array))
        oldRMSE = np.inf
        E_ref = get_energies_atomic(mols=self.GAPclass.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
        scc_training = False
        count_iter = 0
        training_on = True
        Xback = self.kQEqclass._get_Xback()
        X = self.kQEqclass.X_train
        A,Abar,Ainv = self.kQEqclass._build_As(self.kQEqclass.Kernel.training_set)
        O = self.kQEqclass.O_train
        KnmGAP = np.hstack(self.GAPclass.K_nms)
        KnmkQEq = np.hstack(self.kQEqclass.K_nms)
        weightskQEq = self.kQEqclass.weights
        eneg = np.matmul(KnmkQEq,weightskQEq)#/Hartree
        eneg_tot = (np.matmul(X,np.transpose(eneg)))
        eneg_tot = eneg_tot + O
        charge_temp = np.matmul(Ainv,-eneg_tot)
        q_ref_iterative= np.matmul(Xback,charge_temp)
        print(q_ref_iterative[:20])
        bad = 1
        scc_training = False
        if "forces" in iterative_targets_current:
            K_GAP = []
            dK_GAP = []
            K_kQEq = []
            dK_kQEq  = []
            for mol in self.GAPclass.Kernel.training_set:
                Ks,dKdrs  = self.GAPclass.Kernel._calculate_function(mol_set1 = mol)
                K_GAP.append(Ks)
                dK_GAP.append(dKdrs)
            for mol in self.kQEqclass.Kernel.training_set:
                Ks,dKdrs  = self.kQEqclass.Kernel._calculate_function(mol_set1 = mol)
                K_kQEq.append(Ks)
                dK_kQEq.append(dKdrs)
        while training_on:
            if verbose == True:
                print("####################################################################")
            for id_target in range(0,len(iterative_targets_current)):
                if iterative_targets_current[id_target] == "energy":
                    #######kQEq part ###################################################
                    L, b_plus,ref = self.kQEqclass._process_data_iterative(iterative_targets_current[id_target],q_ref_iterative,atom_energy,energy_keyword,forces_keyword)
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    try:
                        b_up_kQEq = np.concatenate((b_up_kQEq,(-ref + b_plus)*diag_sigmas_sqrt))
                        B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([KnmkQEq.T,L])
                        B_up_kQEq = np.concatenate((B_up_kQEq,B_up_temp),axis=1)
                    except:
                        b_up_kQEq = diag_sigmas_sqrt*(-ref + b_plus) 
                        B_up_kQEq = diag_sigmas_sqrt*np.linalg.multi_dot([KnmkQEq.T,L])
                    #######GAP part ###################################################
                    for mol in self.GAPclass.Kernel.training_set:
                        ref = 1*get_energies_atomic(mols=self.GAPclass.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
                        K = np.array(self.GAPclass.Kernel.kernel_matrices(mol_set1 = [mol],kerneltype="predicting"))
                        LK = np.sum(K,axis=1)
                        try:
                            allKLsenergy = np.concatenate([allKLsenergy,np.sum(K,axis=1)],axis=0)
                        except:
                            allKLsenergy = np.sum(K,axis=1)
                    diag_sigmas_sqrt = np.sqrt((1/target_sigmas[id_target]))*np.ones(len(ref))
                    b_up_GAP = diag_sigmas_sqrt*ref 
                    B_up_GAP = diag_sigmas_sqrt*allKLsenergy.T
                elif iterative_targets_current[id_target] == "forces":
                    #######kQEq part ###################################################
                    allKLsforces = None
                    current_position_for_force = 0
                    qeq_forces = []
                    for id_mol,mol in enumerate(self.kQEqclass.Kernel.training_set):
                        current_charges = q_ref_iterative[current_position_for_force:current_position_for_force+len(mol)]                         
                        current_position_for_force += len(mol)
                        # Ks,dKdrs  = self.kQEqclass.Kernel._calculate_function(mol_set1 = mol)
                        Ks = K_kQEq[id_mol]
                        dKs = dK_kQEq[id_mol]
                        qe = charge_eq(mol,Qtot=0,q=current_charges,scale_atsize=self.kQEqclass.scale_atsize,radius_type=self.kQEqclass.radius_type, hard=self.kQEqclass.hard_lib,periodic = self.kQEqclass.periodic)
                        # qe.calc_Charges()
                        qe.calc_Fqeq()
                        qeq_forces.extend((qe.f).flatten())#*(Hartree/Bohr))
                        dKdrs = np.array(dKdrs)
                        current_LK = np.einsum("kadAe,A->kade",dKdrs,current_charges) #k - number of kernels, a - soap derivatives of these atom, d - direction (3), A - wrt to this charges, e - number of weigts
                        current_LK = current_LK.reshape(-1,dKdrs.shape[-1])

                        try:
                            allKLsforces = np.concatenate((allKLsforces,current_LK))
                        except:
                            allKLsforces = current_LK

                    ref = -1*get_forces_atomic(mols=self.kQEqclass.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                    diag_sigmas_sqrt = np.sqrt((1/iterative_targets_current_sigmas[id_target]))*np.ones(len(ref))
                    b_up_temp = diag_sigmas_sqrt*(-ref - np.array(qeq_forces))
                    B_up_temp = diag_sigmas_sqrt*allKLsforces.T
                    # print("forces test",B_up_temp.shape,b_up_temp.shape,diag_sigmas_sqrt.shape,allKLsforces.shape)
                    try:
                        b_up_kQEq = np.concatenate((b_up_kQEq,b_up_temp))
                        # B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                        B_up_kQEq = np.concatenate((B_up_kQEq,B_up_temp),axis=1)
                    except:
                        B_up_kQEq = b_up_temp 
                        B_up_kQEq = B_up_temp #diag_sigmas_sqrt*np.linalg.multi_dot([bigKnm.T,L])
                    #######GAP part ###################################################
                    allKLsforces = None
                    for id_mol,mol in enumerate(self.GAPclass.Kernel.training_set):
                        # Ks,dKdrs  = self.GAPclass.Kernel._calculate_function(mol_set1 = mol)
                        Ks = K_GAP[id_mol]
                        dKdrs = dK_GAP[id_mol]
                        current_dKdrs = np.array(dKdrs)
                        current_LK = np.einsum("abcde->abce",dKdrs).reshape(-1,current_dKdrs.shape[-1])
                        try:
                            allKLsforces = np.concatenate((allKLsforces,current_LK))
                        except:
                            allKLsforces = current_LK
                    # ref = -1*get_forces_eV(mols=self.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                    ref = -1*get_forces_atomic(mols=self.GAPclass.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                    diag_sigmas_sqrt = np.sqrt((1/target_sigmas[id_target]))*np.ones(len(ref))
                    b_up_temp = diag_sigmas_sqrt*(ref)
                    B_up_temp = diag_sigmas_sqrt*allKLsforces.T
                    try:
                        B_up_GAP = np.concatenate((B_up_GAP,B_up_temp),axis=1)
                        b_up_GAP = np.concatenate((b_up_GAP,b_up_temp))
                    except:
                        B_up_GAP = B_up_temp
                        b_up_GAP = b_up_temp                    

            #training_on = False
            # b_up_both = np.concatenate((b_up_GAP,b_up_kQEq))
            if scc_training:
                L, b_plus,ref = self.kQEqclass._process_data_iterative("scc",q_ref_iterative,atom_energy,energy_keyword,forces_keyword)
                diag_sigmas_sqrt = np.sqrt((1/scc_sigma))*np.ones(len(ref))
                b_up_kQEq = np.concatenate((b_up_kQEq,(ref + b_plus)*diag_sigmas_sqrt))
                B_up_temp = diag_sigmas_sqrt*np.linalg.multi_dot([KnmkQEq.T,L])
                B_up_kQEq = np.concatenate((B_up_kQEq,B_up_temp),axis=1)
                scc_sigma = scc_sigma*0.5
                B_up_GAP = np.concatenate((B_up_GAP,np.zeros_like(B_up_temp)),axis=1)
            
            # print(B_up_GAP.shape,B_up_kQEq.shape)
            print(B_up_GAP.shape,B_up_kQEq.shape)
            B_up_both = np.vstack((B_up_GAP,B_up_kQEq))
            Umm = np.linalg.cholesky(bothKmm).T
            B=np.concatenate((B_up_both.T,Umm))
            # print(b_up_both)
            b = np.concatenate((b_up_kQEq,np.zeros(bothKmm.shape[0])))
            # print("Umm",Umm.shapei)
            Q,R = np.linalg.qr(B) #,mode="complete")
            Qb = np.dot(Q.T,b)
            weights = -np.linalg.solve(R,Qb)
            final_weight = weights
            weightsGAP = weights[:KnmGAP.shape[1]]
            weightskQEq = weights[KnmGAP.shape[1]:]
            eneg = np.matmul(KnmkQEq,weightskQEq)#/Hartree
            eneg_tot = (np.matmul(X,np.transpose(eneg)))
            eneg_tot = eneg_tot + O
            charge_temp = np.matmul(Ainv,-eneg_tot)
            q_new = np.matmul(Xback,charge_temp)
            Qmatrix_temp = ones_for_Qmatrix*q_new
            Cs_temp = 0.5*Qmatrix_temp@Abar@Qmatrix_temp.T
            Cs_temp = Cs_temp.sum(axis=0)
            Ekqeq =  Cs_temp + np.linalg.multi_dot([Qmatrix_temp,eneg])
            Egap = np.linalg.multi_dot([ones_for_Qmatrix,KnmGAP,weightsGAP])
            whole_energy = Egap + Ekqeq
            # print(Ekqeq)
            MSE = np.square(np.subtract(E_ref,whole_energy)).mean()
            RMSE_E = np.sqrt(MSE)
            B_up_both = None
            B_up_kQEq = None
            B_up_GAP = None
            b_up_both = None
            b_up_kQEq = None
            b_up_GAP = None
            B = None
            b = None
            allKLsenergy = None
            target_sigmas[1] = 0.5*target_sigmas[1]
            ###########################
            if verbose == True:
                print("New RMSE:",RMSE_E)
                print("Old RMSE:",oldRMSE)
                print("Bad RMSEs in row:", bad)
                print("Sample of charges:",q_new[-10:])
                print("sum of charges: ",np.sum(q_new))
                print(f"iteration {count_iter} is done")
                print("####################################################################")
            if (RMSE_E > oldRMSE and bad >= n_bad_iters) or (count_iter == max_iter):
                training_on = False
                final_weight = weights
            elif RMSE_E > oldRMSE and bad < n_bad_iters and scc_training == False:
                bad += 1
                q_ref_iterative = q_new
                # final_weight = weights # delete this later
                if scc_target == True:
                    print("Activating scc")
                    scc_training = True
            elif RMSE_E > oldRMSE and bad < n_bad_iters and scc_training == True:
                bad += 1
                q_ref_iterative = q_new
            else:
                print("New best model reached")
                bad = 1
                final_weight = weights
                oldRMSE = RMSE_E
                q_ref_iterative = q_new
                
            count_iter += 1
        print("Lenght of weights:",len(final_weight))
        self.weights = final_weight
        self.GAPclass.weights = final_weight[:KnmGAP.shape[1]]
        self.kQEqclass.weights = final_weight[KnmGAP.shape[1]:]




    def calculate(self, mol, charge=0):
        reskQEq = self.kQEqclass.calculate(mol,charge)
        resGAP = self.GAPclass.calculate(mol)
        charges = reskQEq["charges"]
        energy = reskQEq["energy"] + resGAP["energy"]
        forces = reskQEq["forces"] + resGAP["forces"]
        # print(reskQEq["energy"],resGAP["energy"])
        results = {'charges':charges,'energy':energy,"forces":forces}
        return results        
        
    def buildKs(self,kernel_type = "training"):
        if kernel_type == "training":
            KnmGAP = np.hstack(self.GAPclass.K_nms)
            KnmkQEq = np.hstack(self.kQEqclass.K_nms)
            Knm = np.hstack([KnmGAP,KnmkQEq])
            block = []
            row = []
            for k in range(len(self.GAPclass.K_mms)):
                row.append(np.zeros(self.GAPclass.K_mms[k].shape))
            for k in range(len(self.GAPclass.K_mms)):
                block.append(row)
                block[k][k] = self.GAPclass.K_mms[k]
            KmmGAP = np.block(block)
            row = []
            block = []
            for k in range(len(self.kQEqclass.K_mms)):
                row.append(np.zeros(self.kQEqclass.K_mms[k].shape))
            for k in range(len(self.kQEqclass.K_mms)):
                block.append(row)
                block[k][k] = self.kQEqclass.K_mms[k]
            KmmkQEq = np.block(block)                
            zeros1 = np.zeros((KmmGAP.shape[0],KmmkQEq.shape[1]))
            zeros2 = np.zeros((KmmkQEq.shape[0],KmmGAP.shape[1]))

            Kmm = np.block([[KmmGAP,zeros1],[zeros2,KmmkQEq]])
            # Kmm = np.block([[KmmGAP,np.zeros(KmmkQEq.shape)],[np.zeros(KmmGAP.shape),KmmkQEq]])
            return Knm, Kmm
        
    def buildLgap(self):
        L = np.zeros((self.GAPclass.Kernel.nAt_train,len(self.GAPclass.Kernel.training_set)))
        count_c = 0
        count_r = 0
        for ids,mol in enumerate(self.GAPclass.Kernel.training_set):
            q_temp = []
            for q in mol: 
                L[count_r,count_c] = 1
                count_r += 1
            count_c += 1
        return L

