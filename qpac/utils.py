import random
import numpy as np
from ase.io import *
from ase.data import covalent_radii
from ase.units import Bohr,Hartree
from qpac.qeq import charge_eq
from qpac.data import uff_radius_qeq


def CURdecomposition(sparse_count,mat = None, k=None):
        """
        Function for selection and creation of representative set based on CUR decomposition 
        
        
        Parameters
        ----------
        sparse_count : 
            number of data points in represetative set
            
        Returns
        -------
        picked : list 
            list of index for representative set 
            
        """
        #if k == None:
        k = int(np.linalg.matrix_rank(mat)/2) # not doable for multi soap
        if k == 0:
                k = 1
        mat = np.array(mat).T
        _,_,V = np.linalg.svd(mat) # not doable for multi soap
        V = V[:k,:]
        vk = V.T
        pis = (1/k)*np.sum(vk**2,axis=1)
        p = [min(1,a*sparse_count) for a in pis]
        picked = np.flip(np.argsort(p))[:sparse_count]
        # self.representing_set_descriptors = [self.training_set_descriptors[pos] for pos in picked]
        # self.sparse = True
        return picked

def addPeriodicity(mols):
    for mol in mols:
        xyz = mol.get_positions()
        x = abs(max(xyz[:,0]) - min(xyz[:,0]))
        y = abs(max(xyz[:,1]) - min(xyz[:,1]))
        z = max(xyz[:,2]) -  min(xyz[:,2])
        mol.set_cell((x*10, y*10, z*10))
        mol.set_pbc((True, True, True))
        mol.center()

def prep_structures(dataset,NTrain, NVal, NTest,seed = 20):
    """Helper function create sets from the list of ase objects

    Parameters
    ----------
        mols : list
            List of ASE atoms objects with initial charges
        NTrain : int
            Number of molecules in the training set
        NVal : int
            Number of molecules in the validation set
        NTest : int
            Number of molecules in the testing set

    Returns
    -------
        train_set, valid_set, test_set
        np.array
            Numpy array containing the charges
    """
    nmols = len(dataset)
    random.seed(seed) 
    random.shuffle(dataset)
    train_set = dataset[:NTrain]
    valid_set = dataset[NTrain:NTrain+NVal]
    test_set = dataset[NVal+NTrain:NVal+NTrain+NTest]
    
    return train_set, valid_set, test_set

def get_charges(mols,charge_keyword):
    """Helper function to get 1D array of atomic charges from list of atoms objects.

    Parameters
    ----------
        mols : list
            List of ASE atoms objects with initial charges
    Returns
    -------
        np.array
            Numpy array containing the charges
    """
    ref_qs = []
    for mol in mols:
        ref_qs.extend(mol.arrays[charge_keyword])    
    return np.array(ref_qs)
    
def save_charges(mols, charges,charge_keyword = "kqeq_charge",name = "test.xyz"):
    
    count = 0
    for mol in mols:
        ref_qs = charges[count:count+len(mol)]
        count += len(mol)
        mol.arrays[charge_keyword] = np.array(ref_qs)     
    write(name,mols)

def save_file(mols, atom_energies = None,energy=None,forces=None,charges=None,energy_keyword = "kqeq_energy",forces_keyword = "kqeq_forces",charges_keyword = "kqeq_charge",name = "kqeq_results.xyz"):
    count = 0
    for id_mol,mol in enumerate(mols):
        if energy != None:
            mol.info[energy_keyword] = energy[id_mol]*len(mols[id_mol])
            mol.info[f"{energy_keyword}_per_at"] = energy[id_mol]
            at_en = 0
            if atom_energies != None:
                for element in atom_energies:
                    no_of_A = np.count_nonzero(mol.symbols == element)
                    at_en += (no_of_A*atom_energies[element])
                mol.info[f"{energy_keyword}_whole"] = energy[id_mol]*len(mols[id_mol])+at_en
        if forces != None:
            ref_fs = forces[count:count+len(mol)]
            mol.arrays[forces_keyword] = np.array(ref_fs)
        if charges != None:   
            ref_qs = charges[count:count+len(mol)]
            mol.arrays[charges_keyword] = np.array(ref_qs)     
        count += len(mol)
    write(name,mols)

def get_dipoles(mols,dipole_keyword):
    """Helper function to get 1D array of dipole vector elements from list of atoms objects.

    Parameters
    ----------
        mols : list
            List of ASE atoms objects with 'dipole_vector' in info (atomic units!)
    Returns
    -------
        np.array
            Numpy array containing the dipole vector elements
    """
    ref_mus = []
    for mol in mols:
        ref_mus.extend(mol.info[dipole_keyword])
    
    return np.array(ref_mus)

def get_energies_atomic(mols, atom_energy, energy_keyword = "energy"):
    """Function to get atomization energy. This function is used for training on energy, so energy is 
    returned in Hartrees. Dictionary of energies of atoms in vacuum is needed.    

    Parameters
    ----------
        mols : list
            list of ASE atoms with energies
        atom_energy : dictionary
            dictionary of energies of atoms in vacuum
        energy_keyword : string
            word used for energy extraction (energies has to be in eV) 

    Returns
    -------
        np.array
            Numpy array containing the energies in Hartree
    """
    energy = []
    for mol in mols:
        mol_energy = mol.info[energy_keyword]
        for element in atom_energy:
            no_of_A = np.count_nonzero(mol.symbols == element)
            mol_energy -= (no_of_A*atom_energy[element])
        energy.append(mol_energy/Hartree)
    return np.array(energy)

def get_energies_eV(mols, atom_energy, energy_keyword = "energy"):
    """Function to get atomization energy. This function is used for training on energy, so energy is 
    returned in Hartrees. Dictionary of energies of atoms in vacuum is needed.    

    Parameters
    ----------
        mols : list
            list of ASE atoms with energies
        atom_energy : dictionary
            dictionary of energies of atoms in vacuum
        energy_keyword : string
            word used for energy extraction (energies has to be in eV) 

    Returns
    -------
        np.array
            Numpy array containing the energies in Hartree
    """
    energy = []
    for mol in mols:
        mol_energy = mol.info[energy_keyword]
        for element in atom_energy:
            no_of_A = np.count_nonzero(mol.symbols == element)
            mol_energy -= (no_of_A*atom_energy[element])
        energy.append(mol_energy)
    return np.array(energy)


def get_energies_perAtom_eV(mols, atom_energy, energy_keyword = "energy"):
    """Function to get atomization energy per atom. This function is used for comparing results
    from kQEq, so energy is returned in eV. Dictionary of energies of atoms in vacuum is needed.    

    Parameters
    ----------
        mols : list
            list of ASE atoms with energies
        atom_energy : dictionary
            dictionary of energies of atoms in vacuum
        energy_keyword : string
            word used for energy extraction (energies has to be in eV) 

    Returns
    -------
        np.array
            Numpy array containing the atomization energies per atom in eV
    """
    energy = []
    for mol in mols:
        mol_energy = mol.info[energy_keyword]
        for element in atom_energy:
            no_of_A = np.count_nonzero(mol.symbols == element)
            mol_energy -= (no_of_A*atom_energy[element])
        mol_energy = mol_energy#/Hartree
        energy.append(mol_energy/len(mol))
    return energy

def get_forces_atomic(mols, forces_keyword = "forces"):
    forces = []
    for mol in mols:
        forces.extend(mol.arrays[forces_keyword])
    forces =np.array(forces)
    forces = np.array(forces/(Hartree/Bohr))
    return forces

def get_forces_eV(mols, forces_keyword = "forces"):
    forces = []
    for mol in mols:
        forces.extend(mol.arrays[forces_keyword])
    forces = np.array(forces)
    return forces

def get_whole_energies(mols, energy_keyword = "energy"):
    """Function to get energies of systems. Energy is returned in eV.    

    Parameters
    ----------
        mols : list
            list of ASE atoms with energies
        energy_keyword : string
            word used for energy extraction (energies has to be in eV) 

    Returns
    -------
        np.array
            Numpy array containing the energies in eV
    """
    energy = []
    for mol in mols:
        mol_energy = mol.info[energy_keyword]
        energy.append(mol_energy)
    return np.array(energy)#/Hartree

def get_whole_energies_perAtom(mols, energy_keyword = "energy"):
    """Function to get energies of systems per atom. Energy is returned in eV.    

    Parameters
    ----------
        mols : list
            list of ASE atoms with energies
        energy_keyword : string
            word used for energy extraction (energies has to be in eV) 

    Returns
    -------
        np.array
            Numpy array containing the energies per atom in eV
    """
    energy = []
    for mol in mols:
        mol_energy = mol.info[energy_keyword]
        energy.append(mol_energy/len(mol))
    return energy

def _get_R(tmpmol):
    """Helper function to get the R matrix of COM shifted coordinates

           R translates a vector of charges into the dipole vector (in atomic units)
           R*q -> dipole

    Parameters
    ----------
        tmpmol : obj
            ASE atoms objects with initial charges

    Returns
    -------
        np.array
            Numpy array containing the transformation matrix
    """
    mol = tmpmol.copy()
    Nat = len(mol)
    com = mol.get_center_of_mass()
    mol.translate(-com)
    R = mol.get_positions()
    #q = mol.get_initial_charges()
    return np.transpose(R/Bohr)


def concB(all_Bs,dim1,dim2):
    all_matrices = np.vstack((all_Bs))
    big_block_matrix = np.zeros((dim1, dim2))
    desired_shape = (dim1, dim2)
    num_rows, num_cols = all_matrices.shape
    for i in range(dim1):
        for j in range(dim2):
            big_block_matrix[i, j] = all_matrices[(i * dim2[1] + j) % num_rows, (i * dim2[1] + j) % num_cols]
    return big_block_matrix

def _block_diag_rect(all_rs,dim1,dim2):
    """Helper function to construct blocked matrix from list of rectangular matrices.

    Parameters
    ----------
        all_rs : list
            List of rectangular matrices
        dim1: int
            First dimension of blocked matrix
        dim2: int
            Second dimension of blocked matrix

    Returns
    -------
        np.array
            Numpy array containing the blocked matrix
    """
    finalMatrix = np.zeros((dim2,dim1))
    # print(finalMatrix.shape)
    counti = 0
    countj = 0

    for i,r in enumerate(all_rs):
        for ir in range(r.shape[0]):
            for jr in range(r.shape[1]):
                finalMatrix[ir+counti,jr+countj] = r[ir,jr]
        counti += r.shape[0]
        countj += r.shape[1]
    return finalMatrix

def _block_diag_rectR(all_rs,dim1,dim2):
    """Helper function to construct blocked matrix from list of rectangular matrices.

    Parameters
    ----------
        all_rs : list
            List of rectangular matrices
        dim1: int
            First dimension of blocked matrix
        dim2: int
            Second dimension of blocked matrix

    Returns
    -------
        np.array
            Numpy array containing the blocked matrix
    """
    finalMatrix = np.zeros((dim2,dim1))
    print(finalMatrix.shape)
    counti = 0
    countj = 0

    for i,r in enumerate(all_rs):
        for ir in range(r.shape[0]):
            for jr in range(r.shape[1]):
                finalMatrix[ir+counti,jr+countj] = r[ir,jr]
        counti += r.shape[0]
        countj += r.shape[1]+1
    return finalMatrix

def _block_diag(all_as,dim):
    """Helper function to construct blocked matrix from list of square matrices.

    Parameters
    ----------
        all_as : list
            List of square matrices
        dim: int
            Dimension of blocked matrix

    Returns
    -------
        np.array
            Numpy array containing the blocked matrix
    """
    A_bar = np.zeros((dim,dim))
    counti = 0
    countj = 0

    for i,a in enumerate(all_as):
        for ia in range(a.shape[0]):
            for ja in range(a.shape[1]):
                A_bar[ia+counti,ja+countj] = a[ia,ja]
        counti += a.shape[0]
        countj += a.shape[1]
    return A_bar

def calc_gamma(a1,a2):
    return 1.0/np.sqrt(a1**2+a2**2)

def RMSE(kqeq,ref):
    squared_differences = (kqeq-ref) ** 2
    mean_squared_error = squared_differences.mean()
    rmse = np.sqrt(mean_squared_error)
    return rmse

def stack_matrices_diagonally(K):
    dim1 = 0
    dim2 = 0
    for mat in K:
        dim1 += mat.shape[0]
        dim2 += mat.shape[1]
    bigK = np.zeros((dim1,dim2))
    start1,start2,end1,end2 = 0,0,0,0
    for mat in K:
        end1 += mat.shape[0]
        end2 += mat.shape[1]
        bigK[start1:end1,start2:end2] = mat
        start1 = end1
        start2 = end2
    return bigK

# def stack_matrices_diagonally(nD,M):
# nD = len(self.Kernel.Descriptors)
    # dim1 = M.shape[0]*nD
    # dim2 = M.shape[1]*nD
    # bigM = np.zeros((dim1,dim2))
    # start1,start2,end1,end2 = 0,0,0,0
    # for mat in range(nD):
        # end1 += M.shape[0]
        # end2 += M.shape[1]
        # bigM[start1:end1,start2:end2] = M
        # start1 = end1
        # start2 = end2
    # return bigM

def create_L_matrix(index_of_ones):
    total_cols = sum(index_of_ones)
    matrix = np.zeros((len(index_of_ones), total_cols))
    start_index = 0
    for i, num in enumerate(index_of_ones):
        matrix[i, start_index:start_index+num] = 1.0
        start_index += num
    return matrix
