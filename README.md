# kQEq model for SPICE
This is a repo containing pretrained kQEq model for a subset of pubchem conformations in SPICE dataset.

Install ase, numpy, scipy, dscribe, and quippy. 

Files here:
- train.py - file used for the training
- readSPICE.ipynb - notebook for generating used subset
- predict.ipynb - This is probably why you are here. The file for final predictions, and examples how to do batch prediction, and predictions for a single conformer. 